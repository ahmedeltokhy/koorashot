<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Traits\MatchTrait;
class MatchResource extends JsonResource
{
    use MatchTrait;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
                'id'=>$this->id,
                'start_date'=>$this->date,
                'start_time'=>$this->time,
                'minutes_passed'=>"$this->minutes_passed" ??'',
                'status'=>$this->status,
                'competition'=>$this->league?$this->league->name_en:'',
                'competition_id'=>$this->league?$this->league->id:'',
                'localteam'=>$this->local_team?$this->local_team->name_en:'',
                'localteam_id'=>$this->local_team?$this->local_team->id:'',
                'visitorteam'=>$this->visitor_team?$this->visitor_team->name_en:'',
                'visitorteam_id'=>$this->visitor_team?$this->visitor_team->id:'',
                'events'=> MatchEventResource::collection($this->events),
            ];
      
       
        
    }
}
