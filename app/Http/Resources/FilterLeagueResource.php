<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FilterLeagueResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return ['id'=>$this->id,
                'type'=> 'league',
                'name'=>$this->name??'',
                'name_en'=>$this->name_en??'',
                'cover'=>$this->logo_path,
                'filters'=>FilterResource::collection($this->filters),
                ];
    }
}
