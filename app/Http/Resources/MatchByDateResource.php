<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MatchByDateResource extends JsonResource
{
    
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
        return [
            
            'id'=>$this->id,
           'start_date'=>$this->date,
           'start_time'=>$this->time,
           'minutes_passed'=>"$this->minutes_passed" ??'',
           'status'=>$this->status,
           'competition'=>new LeagueResource($this->league),
           'localteam'=>new TeamResource($this->local_team),
           'localteam_score'=>$this->localteam_score??0,
           'visitorteam'=>new TeamResource($this->visitor_team),
           'visitorteam_score'=>$this->visitorteam_score??0
            
        ];
    }
}
