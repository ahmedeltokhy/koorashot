<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NewsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return ['id'=>$this->id,
                'title'=>$this->title,
                'body'=>$this->body ??'',
                'link'=>$this->link??'',
                'image'=>$this->image_path??null,
                'team'=>$this->team_id?$this->team->name_en:'',
                'team_id'=>$this->team_id??'',
                'league'=>$this->league_id?$this->league->name_en:'',
                'league_id'=>$this->league_id??'',
                'source'=>$this->source_id?$this->source->name_en:'',
                'source_id'=>$this->source_id??'',
                'created_at'=>$this->created_at?$this->created_at->format('Y-m-d'):'',
                'publish_date'=>$this->publish_date??'',
             ];
    }
}
