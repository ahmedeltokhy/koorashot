<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;

class TeamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $isFollowed=false; $notification_status=false;
        $timeline_status=false; $news_status=false;
        $user=$request->get('theUser');
        if($user){
            foreach($user->teams as $team){
                if($this->id==$team->id){
                    $isFollowed=true;
                    $notification_status=$team->pivot->notification?true:false;
                    $timeline_status=$team->pivot->timeline?true:false;
                    $news_status=$team->pivot->news?true:false;
                }
            }
        }

       
        return [
                'id'=>$this->id,
                'name'=>$this->name_en,
                'flag'=>$this->logo,
                'is_followed'=>$isFollowed,
                'notification_status'=>$notification_status,
                'timeline_status'=>$timeline_status,
                'news_status'=>$news_status,
                'current_season_id'=>$this->current_season??'',
                'current_season_name'=>$this->season?$this->season->name:'',
                
               ];
    }
}
