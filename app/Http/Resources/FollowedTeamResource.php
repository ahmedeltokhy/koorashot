<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FollowedTeamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
                'id'=>$this->id,
                'name'=>$this->name_en,
                'flag'=>$this->logo,
                'notification_status'=>$this->pivot->notification??0,
                'news_status'=>$this->pivot->news??0,
                 'timeline_status'=>$this->pivot->timeline??0,
                  ];
    }
}
