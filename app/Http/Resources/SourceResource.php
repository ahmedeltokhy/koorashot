<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SourceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $isFollowed=false;
        $user = $request->get('theUser');
        if($user){
            foreach($user->sources as $source){
                if($this->id==$source->id){
                    $isFollowed=true;
                }
            }
        }
        return [
            'id'=>$this->id,
            'name_en'=>$this->name_en,
            'name'=>$this->name??'',
            'logo'=>$this->logo_path?url('public/uploads/sources/'.$this->logo_path) :'',
            'is_followed'=>$isFollowed,
        ];
    }
}
