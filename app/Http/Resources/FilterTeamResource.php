<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FilterTeamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $isLocalTeam=false;
        $local_id=$request->get('localId');
        if($this->id==$local_id){
            $isLocalTeam=true;
        }
        return ['id'=>$this->id,
                'type'=> 'team',
                'isLocalTeam'=>$this->when($local_id,$isLocalTeam),
                'name'=>$this->name??'',
                'name_en'=>$this->name_en??'',
                'cover'=>$this->logo,
                'filters'=>FilterResource::collection($this->filters),
                ];
    }
}
