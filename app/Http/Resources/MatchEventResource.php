<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MatchEventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
                'id'=>$this->id,
                'type'=>$this->type,
                'minute'=>$this->minute,
                'result'=>$this->result,
                'player_name'=>$this->player_name,
                'related_player_name'=>$this->related_player_name,
                'team'=>new TeamResource($this->team),

                
                ];
    }
}
