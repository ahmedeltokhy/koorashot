<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use DB;
use Exception;
use Illuminate\Http\Response;



class CheckApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //  if(!$request->bearerToken()){
        //    return response()->json(['status'=>Response::HTTP_EXPECTATION_FAILED, 'message'=>'please use bearerToken in header' ,'data'=>[]]);
        // }
        $bearerToken = $request->bearerToken();
        $user=null;
        if($bearerToken){
            try{
            $tokenId = (new \Lcobucci\JWT\Parser())->parse($bearerToken)->getHeader('jti');
            
            }catch(Exception $e){
                goto meragePoint;
            }
            $user_id=DB::table('oauth_access_tokens')->where('id',$tokenId)->value('user_id');
            $user=User::find($user_id);
        }
        meragePoint:
        $request->merge(["theUser" => $user]);
        return $next($request);
    }
}
