<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSeasonRequest;
use App\Http\Requests\UpdateSeasonRequest;
use App\Http\Resources\Admin\SeasonResource;
use App\Season;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SeasonApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('season_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new SeasonResource(Season::with(['league'])->get());
    }

    public function store(StoreSeasonRequest $request)
    {
        $season = Season::create($request->all());
        $season->league()->sync($request->input('league', []));

        return (new SeasonResource($season))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Season $season)
    {
        abort_if(Gate::denies('season_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new SeasonResource($season->load(['league']));
    }

    public function update(UpdateSeasonRequest $request, Season $season)
    {
        $season->update($request->all());
        $season->league()->sync($request->input('league', []));

        return (new SeasonResource($season))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Season $season)
    {
        abort_if(Gate::denies('season_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $season->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
