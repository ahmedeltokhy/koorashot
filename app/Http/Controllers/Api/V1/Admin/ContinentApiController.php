<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Continent;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreContinentRequest;
use App\Http\Requests\UpdateContinentRequest;
use App\Http\Resources\Admin\ContinentResource;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ContinentApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('continent_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ContinentResource(Continent::all());
    }

    public function store(StoreContinentRequest $request)
    {
        $continent = Continent::create($request->all());

        return (new ContinentResource($continent))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Continent $continent)
    {
        abort_if(Gate::denies('continent_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ContinentResource($continent);
    }

    public function update(UpdateContinentRequest $request, Continent $continent)
    {
        $continent->update($request->all());

        return (new ContinentResource($continent))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Continent $continent)
    {
        abort_if(Gate::denies('continent_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $continent->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
