<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserDeviceRequest;
use App\Http\Requests\UpdateUserDeviceRequest;
use App\Http\Resources\Admin\UserDeviceResource;
use App\UserDevice;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UserDeviceApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('user_device_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new UserDeviceResource(UserDevice::with(['user'])->get());
    }

    public function store(StoreUserDeviceRequest $request)
    {
        $userDevice = UserDevice::create($request->all());
        $userDevice->user()->sync($request->input('user', []));

        return (new UserDeviceResource($userDevice))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(UserDevice $userDevice)
    {
        abort_if(Gate::denies('user_device_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new UserDeviceResource($userDevice->load(['user']));
    }

    public function update(UpdateUserDeviceRequest $request, UserDevice $userDevice)
    {
        $userDevice->update($request->all());
        $userDevice->user()->sync($request->input('user', []));

        return (new UserDeviceResource($userDevice))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(UserDevice $userDevice)
    {
        abort_if(Gate::denies('user_device_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $userDevice->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
