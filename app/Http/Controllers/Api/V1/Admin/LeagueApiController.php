<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreLeagueRequest;
use App\Http\Requests\UpdateLeagueRequest;
use App\Http\Resources\Admin\LeagueResource;
use App\League;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class LeagueApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('league_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new LeagueResource(League::with(['country'])->get());
    }

    public function store(StoreLeagueRequest $request)
    {
        $league = League::create($request->all());
        $league->country()->sync($request->input('country', []));

        return (new LeagueResource($league))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(League $league)
    {
        abort_if(Gate::denies('league_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new LeagueResource($league->load(['country']));
    }

    public function update(UpdateLeagueRequest $request, League $league)
    {
        $league->update($request->all());
        $league->country()->sync($request->input('country', []));

        return (new LeagueResource($league))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(League $league)
    {
        abort_if(Gate::denies('league_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $league->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
