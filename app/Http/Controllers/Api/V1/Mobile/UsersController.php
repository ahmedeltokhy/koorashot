<?php

namespace App\Http\Controllers\Api\V1\Mobile;

use App\Http\Controllers\Controller;
use App\User;
use App\UserDevice;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use function App\J_response;
use function App\send_email;
use function App\validate_Api_request;

class UsersController extends Controller
{
    public function register(Request $request)
    {
        try {
            $input = $request->input();
            $validator = validate_Api_request($input, [
                'email' => 'required|email|unique:users,email',
                'phone' => 'required|unique:users,email',
                'password' => 'required',
            ]);
            if ($validator['status'] != 200) {
                return $validator;
            }
            $input['password'] = Hash::make($input['password']);
            $user = User::create($input);
            $token = $user->createToken('')->accessToken;
            return J_response(\Illuminate\Http\Response::HTTP_OK, 'User Created', ['user' => $user, 'token' => $token]);
        } catch (\Exception $e) {
            return J_response(Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage(), $e->getTrace());
        }
    }

    public function register_token(Request $request)
    {
        try {
            $input = $request->input();
            $validator = validate_Api_request($input, [
                'user_token' => 'required',
            ]);
            if ($validator['status'] != 200) {
                return $validator;
            }
            $user_id = $request->user()->id;
            UserDevice::firstOrCreate(['user_id' => $user_id, 'token' => $input['user_token']]);
            return J_response(\Illuminate\Http\Response::HTTP_OK, 'token registered', []);
        } catch (\Exception $e) {
            return J_response(Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage(), $e->getTrace());
        }
    }

    public function logout(Request $request)
    {
        try {
            $user_id = $request->user()->id;
            $user_tokens = UserDevice::where([['user_id', '=', $user_id]])->delete();
            $userInstance = User::find($user_id);
            $userTokens = $userInstance->tokens;

            foreach ($userTokens as $token) {
                $token->revoke();
            }
            return J_response(\Illuminate\Http\Response::HTTP_OK, 'Logged out', []);
        } catch (\Exception $e) {
            return J_response(Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage(), $e->getTrace());
        }
    }

    public function login(Request $request)
    {
        try {
            $input = $request->input();
            $validator = validate_Api_request($input, [
                'email' => 'required',
                'password' => 'required',
            ]);
            if ($validator['status'] != 200) {
                return $validator;
            }
            if (auth()->attempt(['email' => $input['email'], 'password' => $input['password']])) {
                $user = auth()->user();
                $token = $user->createToken('')->accessToken;
                return J_response(\Illuminate\Http\Response::HTTP_OK, 'logged in successfully', ['user' => $user, 'token' => $token]);

            } else {
                return J_response(Response::HTTP_UNAUTHORIZED, 'un Authorized', null);
            }

        } catch (\Exception $e) {
            return J_response(Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage(), $e->getTrace());
        }
    }

    public function remove_token(Request $request)
    {
        try {
            $input = $request->input();
            $validator = validate_Api_request($input, [
                'token' => 'required',
            ]);
            if ($validator['status'] != 200) {
                return $validator;
            }
            $user_id = $request->user()->id;
            $token = UserDevice::where([['user_id', '=', $user_id], ['token', '=', $input['token']]])->delete();
            return J_response(\Illuminate\Http\Response::HTTP_OK, 'token removed', null);

        } catch (\Exception $e) {
            return J_response(Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage(), $e->getTrace());
        }
    }

    public function reset(Request $request)
    {
        try {
            $input = $request->input();
            $validator = validate_Api_request($input, [
                'email' => 'required|email|exists:users,email',
            ]);
            if ($validator['status'] != 200) {
                return $validator;
            }
            $new_pass = random_int(1000, 9999);
            $user = User::where('email', '=', $input['email'])->first();
            $user->password = Hash::make($new_pass);
            $user->save();
            send_email([$input['email']], "New Password:\n$new_pass", 'reset password');
            return J_response(\Illuminate\Http\Response::HTTP_OK, 'password sent', null);

        } catch (\Exception $e) {
            return J_response(Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage(), $e->getTrace());
        }
    }

    public function api_check()
    {
        $country = isset(json_decode(file_get_contents('http://tripteapp.com/public/ip/'.$_SERVER['REMOTE_ADDR']))->country_code) ? (json_decode(file_get_contents('http://tripteapp.com/public/ip/'.$_SERVER['REMOTE_ADDR']))->country_code) : '--';
        if ($country == 'SA') {
            return ["status" => 1, "message" => "ReadyForSale"];
        } else {
            return ["status" => 0, "message" => "InReview"];
        }
    }

    public function download()
    {
        return redirect()->to($this->openLink('https://apps.apple.com/us/app/id1502023346',
            'https://play.google.com/store/apps/details?id=com.koorashot.android',
            'https://apps.apple.com/us/app/id1502023346'));

    }

    public function openLink($iosLink, $androidLink, $globalLink)
    {
        $iPhone = strstr($_SERVER['HTTP_USER_AGENT'], "iPhone");
        $Android = strstr($_SERVER['HTTP_USER_AGENT'], 'Android');

        if ($iPhone) {
//            header("Location:" . $iosLink);
            return $iosLink;
        } else if ($Android) {
//            header("Location:" . $androidLink);
            return $androidLink;
        } else {
//            header("Location:" . $globalLink);
            return $globalLink;
        }
    }
}
