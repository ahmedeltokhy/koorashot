<?php

namespace App\Http\Controllers\Api\V1\Mobile;

use App\Http\Controllers\Controller;
use App\Team;
use App\Season;
use App\League;
use DB;
//resources
use App\Http\Resources\TeamResource;
use App\Http\Resources\FollowedTeamResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use function App\J_response;
use function App\validate_Api_request;

class TeamController extends Controller
{
      /* for teams
    **you can get collection of teams based on[season]
    **by default I get teams of  current active seasons 
    ** and insert it in database
    ** 
    **todo : handling event or cron job to run every season  &anothor event for deleting old teams
    */

    

     /* 
     ** get teams by league
     ** get teams by season 
     */
    public function getTeams(Request $request){
        
        $input = $request->input();
        $validator = validate_Api_request($input, [
            'season_id' => 'exists:seasons,id',
            'league_id' => 'exists:leagues,id',
           
        ]);
        if ($validator['status'] != 200) {
            return $validator;
        }

        
        $current_seasons=DB::table('seasons')->select('id')->where('is_current_season',1)->get();
        getTeamsPoint:
        $teams=[];
        foreach($current_seasons as $season){
            try{
                array_push($teams,Team::where('current_season',$season->id)->get());
            }catch(Exception $e){
                return Team::where('current_season',$season->id)->get();
            }
        }
        $teams=collect($teams)->collapse();
        if(count($teams)==0){
          $this->recordTeams($current_seasons);
          goto getTeamsPoint;
        }
        if($request->season_id){
             $season_id=$request->season_id;
             getSeasonPoint:
            $season=Season::find($season_id);
            if($season->is_current_season!=1){
                //get season's teams from  supplier and insert it in database
                $this->getTeamsBySeasonFromSupplier($season->id);
            }
            $teams=$teams->where('current_season',$season_id);
        // in case of get teams by league
        }elseif($request->league_id){
            $season_id=League::find($request->league_id)->current_season;
            goto getSeasonPoint;
        }
        return J_response(\Illuminate\Http\Response::HTTP_OK, 'Teams Retrieved ', TeamResource::collection($teams));
    

    }
    public function getTeamByName(Request $request){
        $input = $request->input();
        $validator = validate_Api_request($input, [
            'name' => 'string|max:30',
        ]);
        if ($validator['status'] != 200) {
            return $validator;
        }
        $team=Team::where('name_en','like',"%{$request->name}%")->orWhere('name','like',"%{$request->name}%")->get();
        if(!$team){
            return J_response(\Illuminate\Http\Response::HTTP_OK, 'no such team','');
        }
        return J_response(\Illuminate\Http\Response::HTTP_OK, 'Team Retrieved ', TeamResource::collection($team));

    }

    /**
     * follow team protected by check api token middleware
     * get user object from request comes from middleware 
     */
    public function followTeam(Request $request){
        $input = $request->input();
        $validator = validate_Api_request($input, [
           'team_id' => 'required|exists:teams,id',
           'follow_action'=>'required|in:add,remove',
           'notification'=>'boolean',
           'timeline'=>'boolean',
           'news'=>'boolean',
        ]);
        if ($validator['status'] != 200) {
            return $validator;
        }
        $user = $request->get('theUser');
        if($user){
            if($request->follow_action=='add'){
                $user->teams()->syncWithoutDetaching([
                    $request->team_id=>['notification'=>$request->notification??0,
                                         'timeline'=>$request->timeline??0,
                                         'news'=>$request->news??0]
                    ]);
            }else{
                $user->teams()->detach($request->team_id);
            }
        }else{
            return J_response(Response::HTTP_EXPECTATION_FAILED, 'user not found', []);
        }
        return J_response(Response::HTTP_OK, 'Action Done ', []);
    }
    //get my teams
    public function myFollowedTeam(Request $request){
       $user = $request->get('theUser');
       if($user){
           $teams=$user->teams;
         }else{
            return J_response(Response::HTTP_EXPECTATION_FAILED, 'user not found', []);
        }
        if(count($teams)==0){
            return J_response(Response::HTTP_OK, 'no followed teams', []);
        }
        return J_response(Response::HTTP_OK, 'Teams Retrieved ', TeamResource::collection($user->teams));
    }


    /*                                      *********************************
    ** help functions 
    ***
    */
    function recordTeams($current_seasons){
        
        foreach($current_seasons as $season){
           $this->getTeamsBySeasonFromSupplier($season->id);
        }
         return "database ready";
     }

    function getTeamsBySeasonFromSupplier($season_id){
        $client = new \GuzzleHttp\Client();
        $current_page=1;$count_pages=1;
        while($count_pages>=$current_page){
            $response = $client->request('GET', 'https://soccer.sportmonks.com/api/v2.0/teams/season/'.$season_id.'?api_token=6780XK6x95dDUxH71gYeQaAzfz0g34hr1S9mSh8H0CYRyhirxW8FNUiA3MwS');
            $teams= json_decode($response->getBody(),true);
            $count_pages= $teams["meta"]["pagination"]["total_pages"];
            foreach($teams["data"] as $team){
                $this->insertIntoTeamsTable($team);
            }
            $current_page++;
        }
    }
  

    function insertIntoTeamsTable($team){
        $is_exist=Team::where('id',$team["id"])->first();
                if($is_exist){
                    $the_team=$is_exist;
                }else{
                    $the_team=new Team;
                    $the_team->id= $team["id"];
                }
                $the_team->name_en= $team["name"];
                $the_team->short_code= $team["short_code"];
                $the_team->twitter= $team["twitter"];
                $the_team->national_team= $team["national_team"]?1:0;
                $the_team->founded= $team["founded"];
                $the_team->logo= $team['logo_path'];
                $the_team->venue= $team['venue_id'];
                $the_team->current_season= $team['current_season_id'];
                $the_team->country_id= $team['country_id'];
                try{
                    $the_team->save();
                }catch(Exception $e){
                    return false;
                }
               

    }
    
   
}
