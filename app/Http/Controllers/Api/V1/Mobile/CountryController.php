<?php

namespace App\Http\Controllers\Api\V1\Mobile;

use App\Http\Controllers\Controller;
use App\Country;
use App\UserDevice;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use function App\J_response;
use function App\send_email;
use function App\validate_Api_request;

class CountryController extends Controller
{
    public function getAllcountries(){
        $client = new \GuzzleHttp\Client();
        $current_page=1;$count_pages=1;
      while($count_pages>=$current_page){
        $response = $client->request('GET', 'https://soccer.sportmonks.com/api/v2.0/countries?api_token=6780XK6x95dDUxH71gYeQaAzfz0g34hr1S9mSh8H0CYRyhirxW8FNUiA3MwS&include=continent&page='.$current_page);
        $countries= json_decode($response->getBody(),true);
        $count_pages= $countries["meta"]["pagination"]["total_pages"];
        foreach($countries["data"] as $con){
            $is_exist=Country::where('id',$con["id"])->first();
            if($is_exist){
                $country=$is_exist;
            }else{
                $country=new Country;
                $country->id= $con["id"];
            }
            $country->name_en= $con["name"];
            $country->flag= $con["image_path"];
           if($con["extra"]){
            $country->sub_region= $con["extra"]["sub_region"];
            $country->world_region= $con["extra"]["world_region"];
            $country->fifa= $con["extra"]["fifa"];
            $country->iso= $con["extra"]["iso"];
            $country->iso_2= $con["extra"]["iso2"];
            $country->lat= $con["extra"]["latitude"];
            $country->lng= $con["extra"]["longitude"];
           }
           if(isset($con["continent"])){
            $country->continent_id=$con["continent"]["data"]["id"];
           }
            $country->save();
        }
        $current_page++;
    }
      
        return true;
    }
   
}
