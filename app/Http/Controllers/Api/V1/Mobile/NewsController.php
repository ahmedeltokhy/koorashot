<?php

namespace App\Http\Controllers\Api\V1\Mobile;

use App\Http\Controllers\Controller;
use App\Team;
use App\News;
use App\League;
use App\Source;
//resources
use App\Http\Resources\AllNewsResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use function App\J_response;
use function App\validate_Api_request;

class NewsController extends Controller
{
    public function getNews(Request $request){
        
        $validator = validate_Api_request($request->input(), [
            'source'=>'string|max:30'
        ]);
        
        if ($validator['status'] != 200) {
            return $validator;
        }
        $user = $request->get('theUser');
        
        if($user){
            $followed_sources=$user->sources->pluck('id');
            if(count($followed_sources)>0){
                $news=News::whereIn('source_id',$followed_sources)->orderBy('created_at','desc')->paginate(20);
            }
        }elseif(!$user ||count($followed_sources)==0 ){
            $news=News::orderBy('created_at','desc')->paginate(20);
        }
        if($request->source){
            $source=Source::where('name_en','like',"%{$request->source}%")->orWhere('name','like',"%{$request->source}%")->first();
            $news=$source?$news->where('source_id',$source->id):collect([]);
        }
      return  new AllNewsResource($news);
    }
   

}