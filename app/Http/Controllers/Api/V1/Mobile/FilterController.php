<?php

namespace App\Http\Controllers\Api\V1\Mobile;

use App\Http\Controllers\Controller;
use App\Filter;
use App\Team;
use App\League;
use App\Match;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use function App\J_response;
use function App\validate_Api_request;
use App\Http\Resources\FilterTeamResource;
use App\Http\Resources\FilterResource;
use App\Http\Resources\FilterLeagueResource;

class FilterController extends Controller
{
    public function getFilters(Request $request){
        $input = $request->input();
        $validator = validate_Api_request($input, [
            'filterable_type' => 'in:team,league',
            'name'=>'string|max:60',
        ]);
        if ($validator['status'] != 200) {
            return $validator;
        }
        $filterable_type=$request->filterable_type;
        $name=$request->name;
        $teams=Team::has('filters')->get();
        $leagues=League::has('filters')->get();
        if($filterable_type){
            if($filterable_type=='team'){
                $leagues=collect([]);
            }else{
                $teams=collect([]);
            }
        }
        if($name){
            $teams=Team::has('filters')->where('name_en','like',"%{$name}%")->orWhere('name','like',"%{$name}%")->get();
            $leagues=League::has('filters')->where('name_en','like',"%{$name}%")->orWhere('name','like',"%{$name}%")->get();
        }
        $leagueFilters=(array) FilterLeagueResource::collection($leagues);
        $teamFilters=(array) FilterTeamResource::collection($teams);
        $data=[];
         foreach($teamFilters['collection'] as $filter){
             array_push($data,$filter);
         }
         foreach($leagueFilters['collection'] as $filter){
             array_push($data,$filter);
         }
        return J_response(\Illuminate\Http\Response::HTTP_OK, 'Filters Retrieved ', collect($data));
   }

   public function getMatchFilters(Request $request){
    $input = $request->input();
    $validator = validate_Api_request($input, [
        'match_id'=>'required|exists:matches,id'
    ]);
    if ($validator['status'] != 200) {
        return $validator;
    }
    $match=Match::find($request->match_id);
    if($match){
        $request->merge(['localId'=>$match->local_team->id]);
        
      $data= [
            $match->local_team?new FilterTeamResource($match->local_team):'',
            $match->visitor_team?new FilterTeamResource($match->visitor_team):'',
            $match->league?new FilterLeagueResource($match->league):'',
        ];
        return J_response(\Illuminate\Http\Response::HTTP_OK, 'Filters Retrieved ', collect($data));

    }
         
   }


}