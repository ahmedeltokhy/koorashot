<?php

namespace App\Http\Controllers\Api\V1\Mobile;

use App\Http\Controllers\Controller;
use App\League;
use App\UserDevice;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use function App\J_response;
use function App\send_email;
use function App\validate_Api_request;

class LeagueController extends Controller
{
    public function recordLeagues(){
         $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'https://soccer.sportmonks.com/api/v2.0/leagues?api_token=6780XK6x95dDUxH71gYeQaAzfz0g34hr1S9mSh8H0CYRyhirxW8FNUiA3MwS&include=country');

        $leagues= json_decode($response->getBody(),true);
        
        foreach($leagues["data"] as $leag){
            $is_exist=League::where('id',$leag["id"])->first();
            if($is_exist){
                $league=$is_exist;
            }else{
                $league=new League;
                $league->id= $leag["id"];
            }
            $league->active=$leag["active"] ?1 :0;
            $league->name_en= $leag["name"];
            $league->logo_path= $leag["logo_path"];
            $league->is_cup= $leag["is_cup"];
            $league->current_season= $leag["current_season_id"];
            $league->current_round= $leag["current_round_id"];
            $league->current_stage= $leag["current_stage_id"];
            $league->live_standings= $leag["live_standings"]?1:0;
            $league->predictions= $leag["coverage"]["predictions"];
            $league->topscorer_assists= $leag["coverage"]["topscorer_assists"] ?1:0;
            $league->topscorer_cards= $leag["coverage"]["topscorer_cards"]?1:0;
            $league->topscorer_goals= $leag["coverage"]["topscorer_goals"]?1:0;
            $league->country_id= $leag["country"]["data"]["id"];
           $league->save();
        }
      
        return "database ready";
         


    }
    public function getLeagues(){
        return League::orderBy('id','desc')->paginate(20);
    }
   
}
