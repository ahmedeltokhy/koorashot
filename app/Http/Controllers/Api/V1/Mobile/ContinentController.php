<?php

namespace App\Http\Controllers\Api\V1\Mobile;

use App\Http\Controllers\Controller;
use App\Continent;
use App\UserDevice;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use function App\J_response;
use function App\send_email;
use function App\validate_Api_request;

class ContinentController extends Controller
{
    public function getAllContinents(){
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'https://soccer.sportmonks.com/api/v2.0/continents?api_token=6780XK6x95dDUxH71gYeQaAzfz0g34hr1S9mSh8H0CYRyhirxW8FNUiA3MwS');
        $Continents= json_decode($response->getBody(),true);
        foreach($Continents["data"] as $Continent){
             $contin=new Continent;
             $contin->id=$Continent["id"];
             $contin->name=$Continent["name"];
             $contin->save();
        }
        return true;
    }
   
}
