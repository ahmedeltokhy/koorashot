<?php

namespace App\Http\Controllers\Api\V1\Mobile;

use App\Http\Controllers\Controller;
use App\Team;
//resources
use App\Http\Resources\MatchResource;
use App\Http\Resources\MatchByDateResource;
//helpers
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use function App\J_response;
use function App\validate_Api_request;
//traits
use App\Traits\MatchTrait;
use App\Traits\DateTrait;
// RepositoryInterface
use App\Repositories\Matches\MatchRepositoryInterface;
class MatchController extends Controller
{
    use MatchTrait , DateTrait ;

    public function getByDateRange(Request $request,MatchRepositoryInterface $matchRepo) {
        $input = $request->input();
        $validator = validate_Api_request($input, [
           'date' => 'required|date',
           'page'=>'integer'
        ]);
        if ($validator['status'] != 200) {
            return $validator;
        }
        $user = $request->get('theUser');
        if($user){
            $followedTeams=$this->getUserFollowedTeamsIds($user);
            $followedTeamsIds=count($followedTeams)>0?$followedTeams:Team::pluck('id');
        }else{
            $followedTeamsIds=Team::pluck('id');
        }
        $dateRange=$this->dateRange($request->date,$request->page);
        $matches=$matchRepo->getByDateRangeForUserFollowedTeams($dateRange['start_date'],$dateRange['end_date'],$followedTeamsIds);
        $data=$this->setDataArray($dateRange['start_date'],$dateRange['end_date']);
        foreach($matches as $match){
            foreach($data as $i=>$data_array){
                if($match->date==$data_array['date']){
                    array_push($data_array['matches'],new MatchByDateResource($match));
                }
                $data[$i]=$data_array;
            }

        } 
        return J_response(Response::HTTP_OK,'Matches Retrieved',$data);
    }

  
    public function getById(Request $request,MatchRepositoryInterface $matchRepo,$id){
       
      return J_response(Response::HTTP_OK,'Match Retrieved',new MatchResource($matchRepo->find($id)));
    }
 
   
   

   
  


   
}
