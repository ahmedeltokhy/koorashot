<?php

namespace App\Http\Controllers\Api\V1\Mobile;

use App\Http\Controllers\Controller;
use App\Team;
use App\News;
use App\League;
use App\Source;
//resources
use App\Http\Resources\SourceResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use function App\J_response;
use function App\validate_Api_request;

class SourceController extends Controller
{
   
    public function getNewsSources(Request $request){
        return J_response(\Illuminate\Http\Response::HTTP_OK, 'news sources Retrieved ',SourceResource::collection(Source::all()));

    }


    public function followSource(Request $request){
        $input = $request->input();
        $validator = validate_Api_request($input, [
           'source_id' => 'required|exists:sources,id',
           'follow_action'=>'required|in:add,remove',
        ]);
        if ($validator['status'] != 200) {
            return $validator;
        }
        $user = $request->get('theUser');
        if($user){ 
            if($request->follow_action=='add'){
                $user->sources()->syncWithoutDetaching($request->source_id);
            }else{
                $user->sources()->detach($request->source_id);
            }
        }else{
            return J_response(Response::HTTP_EXPECTATION_FAILED, 'user not found', []);

        }
      return J_response(\Illuminate\Http\Response::HTTP_OK, 'Action Done ', []);
    }

}