<?php

namespace App\Http\Controllers\Api\V1\Mobile;

use App\Http\Controllers\Controller;
use App\Season;
use App\UserDevice;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use function App\J_response;
use function App\send_email;
use function App\validate_Api_request;

class SeasonController extends Controller
{
    public function recordSeasons(){
        \Cache::flush();
         $client = new \GuzzleHttp\Client();
         $current_page=1;$count_pages=1;
      while($count_pages>=$current_page){
        $response = $client->request('GET', 'https://soccer.sportmonks.com/api/v2.0/seasons?api_token=6780XK6x95dDUxH71gYeQaAzfz0g34hr1S9mSh8H0CYRyhirxW8FNUiA3MwS&include=league&page='.$current_page);
        $seasons= json_decode($response->getBody(),true);
        $count_pages= $seasons["meta"]["pagination"]["total_pages"];
        foreach($seasons["data"] as $seas){
            $is_exist=Season::where('id',$seas["id"])->first();
            if($is_exist){
                $season=$is_exist;
            }else{
                $season=new Season;
                $season->id= $seas["id"];
            }
            $season->name= $seas["name"];
            $season->is_current_season= $seas["is_current_season"]?1:0;
            $season->current_round= $seas["current_round_id"];
            $season->current_stage= $seas["current_stage_id"];
            $season->league_id= $seas["league"]["data"]["id"];
            $season->save();
        }
        $current_page++;
    }
      
        return "database ready";
         


    }
    public function getSeasons(){
        return Season::orderBy('id','desc')->paginate(20);
    }
   
}
