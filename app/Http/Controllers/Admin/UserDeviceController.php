<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyUserDeviceRequest;
use App\Http\Requests\StoreUserDeviceRequest;
use App\Http\Requests\UpdateUserDeviceRequest;
use App\User;
use App\UserDevice;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UserDeviceController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('user_device_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $userDevices = UserDevice::all();

        return view('admin.userDevices.index', compact('userDevices'));
    }

    public function create()
    {
        abort_if(Gate::denies('user_device_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.userDevices.create', compact('users'));
    }

    public function store(StoreUserDeviceRequest $request)
    {
        $userDevice = UserDevice::create($request->all());

        return redirect()->route('admin.user-devices.index');
    }

    public function edit(UserDevice $userDevice)
    {
        abort_if(Gate::denies('user_device_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $userDevice->load('user');

        return view('admin.userDevices.edit', compact('users', 'userDevice'));
    }

    public function update(UpdateUserDeviceRequest $request, UserDevice $userDevice)
    {
        $userDevice->update($request->all());

        return redirect()->route('admin.user-devices.index');
    }

    public function show(UserDevice $userDevice)
    {
        abort_if(Gate::denies('user_device_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $userDevice->load('user');

        return view('admin.userDevices.show', compact('userDevice'));
    }

    public function destroy(UserDevice $userDevice)
    {
        abort_if(Gate::denies('user_device_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $userDevice->delete();

        return back();
    }

    public function massDestroy(MassDestroyUserDeviceRequest $request)
    {
        UserDevice::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
