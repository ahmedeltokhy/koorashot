<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyMatchRequest;
use App\Http\Requests\StoreMatchRequest;
use App\Http\Requests\UpdateMatchRequest;
use App\League;
use App\Match;
use App\Season;
use App\Team;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MatchController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('match_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $matches = Match::all();

        return view('admin.matches.index', compact('matches'));
    }

    public function create()
    {
        abort_if(Gate::denies('match_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $leagues = League::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $seasons = Season::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $local_teams = Team::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $visitor_teams = Team::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $winner_teams = Team::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.matches.create', compact('leagues', 'seasons', 'local_teams', 'visitor_teams', 'winner_teams'));
    }

    public function store(StoreMatchRequest $request)
    {
        $match = Match::create($request->all());

        return redirect()->route('admin.matches.index');
    }

    public function edit(Match $match)
    {
        abort_if(Gate::denies('match_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $leagues = League::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $seasons = Season::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $local_teams = Team::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $visitor_teams = Team::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $winner_teams = Team::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $match->load('league', 'season', 'local_team', 'visitor_team', 'winner_team');

        return view('admin.matches.edit', compact('leagues', 'seasons', 'local_teams', 'visitor_teams', 'winner_teams', 'match'));
    }

    public function update(UpdateMatchRequest $request, Match $match)
    {
        $match->update($request->all());

        return redirect()->route('admin.matches.index');
    }

    public function show(Match $match)
    {
        abort_if(Gate::denies('match_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $match->load('league', 'season', 'local_team', 'visitor_team', 'winner_team');

        return view('admin.matches.show', compact('match'));
    }

    public function destroy(Match $match)
    {
        abort_if(Gate::denies('match_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $match->delete();

        return back();
    }

    public function massDestroy(MassDestroyMatchRequest $request)
    {
        Match::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
