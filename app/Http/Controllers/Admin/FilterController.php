<?php

namespace App\Http\Controllers\Admin;

use App\Filter;
use App\League;
use App\Team;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyFilterRequest;
use App\Http\Requests\StoreFilterRequest;
use App\Http\Requests\UpdateFilterRequest;
use Gate;
use Image;
use Exception;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class FilterController extends Controller
{
    // use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('filter_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $filters = Filter::all();

        return view('admin.filters.index', compact('filters'));
    }

    public function create()
    {
        abort_if(Gate::denies('filter_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $leagues = League::all()->pluck('name_en', 'id')->prepend(trans('global.pleaseSelect'), '');
        $teams = Team::all()->pluck('name_en', 'id')->prepend(trans('global.pleaseSelect'), '');
        return view('admin.filters.create', compact('leagues','teams'));
    }

    public function store(StoreFilterRequest $request)
    {
        $type=$request->imageable_type =='App\Team'?'team':'league';
        $filter=new Filter;
        $filter->imageable_type=$request->imageable_type;
        $filter->imageable_id=$request->imageable_id;
        $img=$request->image;
        if($img){
            $dest=public_path().'/uploads/filters';
            if(!file_exists($dest)){
                mkdir($dest, 0777, true);
            }
            $filename = $type."-" .$request->imageable_id . "-" . time()  . '.' . $img->getClientOriginalExtension();
            try{
               
               Image::make($img)->save($dest.'/'.$filename);
            }catch(Exception $e){
                return $e->getMessage(); 
               
            }
            $filter->url= $filename; 
            
        }
        $filter->save();

        // $filter = Filter::create($request->all());

        // if ($request->input('image', false)) {
        //     $filter->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
        // }

        // if ($media = $request->input('ck-media', false)) {
        //     Media::whereIn('id', $media)->update(['model_id' => $filter->id]);
        // }

        return redirect()->route('admin.filters.index');
    }

    public function edit(Filter $filter)
    {
        abort_if(Gate::denies('filter_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.filters.edit', compact('filter'));
    }

    public function update(UpdateFilterRequest $request, Filter $filter)
    {
        $filter->update($request->all());

        if ($request->input('image', false)) {
            if (!$filter->image || $request->input('image') !== $filter->image->file_name) {
                $filter->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
            }
        } elseif ($filter->image) {
            $filter->image->delete();
        }

        return redirect()->route('admin.filters.index');
    }

    public function show(Filter $filter)
    {
        abort_if(Gate::denies('filter_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.filters.show', compact('filter'));
    }

    public function destroy(Filter $filter)
    {
        abort_if(Gate::denies('filter_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $filter->delete();

        return back();
    }

    public function massDestroy(MassDestroyFilterRequest $request)
    {
        Filter::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('filter_create') && Gate::denies('filter_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Filter();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media', 'public');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
