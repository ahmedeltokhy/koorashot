<?php

namespace App\Http\Controllers\Admin;

use App\Country;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyLeagueRequest;
use App\Http\Requests\StoreLeagueRequest;
use App\Http\Requests\UpdateLeagueRequest;
use App\League;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class LeagueController extends Controller
{
    // use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('league_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $leagues = League::all();

        return view('admin.leagues.index', compact('leagues'));
    }

    public function create()
    {
        abort_if(Gate::denies('league_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $countries = Country::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.leagues.create', compact('countries'));
    }

    public function store(StoreLeagueRequest $request)
    {
        $league = League::create($request->all());

        if ($request->input('logo', false)) {
            $league->addMedia(storage_path('tmp/uploads/' . $request->input('logo')))->toMediaCollection('logo');
        }

        return redirect()->route('admin.leagues.index');
    }

    public function edit(League $league)
    {
        abort_if(Gate::denies('league_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $countries = Country::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $league->load('country');

        return view('admin.leagues.edit', compact('countries', 'league'));
    }

    public function update(UpdateLeagueRequest $request, League $league)
    {
        $league->update($request->all());

        if ($request->input('logo', false)) {
            if (!$league->logo || $request->input('logo') !== $league->logo->file_name) {
                $league->addMedia(storage_path('tmp/uploads/' . $request->input('logo')))->toMediaCollection('logo');
            }
        } elseif ($league->logo) {
            $league->logo->delete();
        }

        return redirect()->route('admin.leagues.index');
    }

    public function show(League $league)
    {
        abort_if(Gate::denies('league_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $league->load('country');

        return view('admin.leagues.show', compact('league'));
    }

    public function destroy(League $league)
    {
        abort_if(Gate::denies('league_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $league->delete();

        return back();
    }

    public function massDestroy(MassDestroyLeagueRequest $request)
    {
        League::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
