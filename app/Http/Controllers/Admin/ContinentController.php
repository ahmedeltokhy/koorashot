<?php

namespace App\Http\Controllers\Admin;

use App\Continent;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyContinentRequest;
use App\Http\Requests\StoreContinentRequest;
use App\Http\Requests\UpdateContinentRequest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ContinentController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('continent_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $continents = Continent::all();

        return view('admin.continents.index', compact('continents'));
    }

    public function create()
    {
        abort_if(Gate::denies('continent_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.continents.create');
    }

    public function store(StoreContinentRequest $request)
    {
        $continent = Continent::create($request->all());

        return redirect()->route('admin.continents.index');
    }

    public function edit(Continent $continent)
    {
        abort_if(Gate::denies('continent_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.continents.edit', compact('continent'));
    }

    public function update(UpdateContinentRequest $request, Continent $continent)
    {
        $continent->update($request->all());

        return redirect()->route('admin.continents.index');
    }

    public function show(Continent $continent)
    {
        abort_if(Gate::denies('continent_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.continents.show', compact('continent'));
    }

    public function destroy(Continent $continent)
    {
        abort_if(Gate::denies('continent_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $continent->delete();

        return back();
    }

    public function massDestroy(MassDestroyContinentRequest $request)
    {
        Continent::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
