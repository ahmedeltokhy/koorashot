<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroySeasonRequest;
use App\Http\Requests\StoreSeasonRequest;
use App\Http\Requests\UpdateSeasonRequest;
use App\League;
use App\Season;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SeasonController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('season_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $seasons = Season::all();

        return view('admin.seasons.index', compact('seasons'));
    }

    public function create()
    {
        abort_if(Gate::denies('season_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $leagues = League::all()->pluck('sm', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.seasons.create', compact('leagues'));
    }

    public function store(StoreSeasonRequest $request)
    {
        $season = Season::create($request->all());

        return redirect()->route('admin.seasons.index');
    }

    public function edit(Season $season)
    {
        abort_if(Gate::denies('season_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $leagues = League::all()->pluck('sm', 'id')->prepend(trans('global.pleaseSelect'), '');

        $season->load('league');

        return view('admin.seasons.edit', compact('leagues', 'season'));
    }

    public function update(UpdateSeasonRequest $request, Season $season)
    {
        $season->update($request->all());

        return redirect()->route('admin.seasons.index');
    }

    public function show(Season $season)
    {
        abort_if(Gate::denies('season_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $season->load('league');

        return view('admin.seasons.show', compact('season'));
    }

    public function destroy(Season $season)
    {
        abort_if(Gate::denies('season_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $season->delete();

        return back();
    }

    public function massDestroy(MassDestroySeasonRequest $request)
    {
        Season::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
