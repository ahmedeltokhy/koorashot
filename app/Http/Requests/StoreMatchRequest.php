<?php

namespace App\Http\Requests;

use App\Match;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreMatchRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('match_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'match'     => [
                'digits_between:0,10',
            ],
            'stage'     => [
                'digits_between:0,10',
            ],
            'round'     => [
                'digits_between:0,10',
            ],
            'group'     => [
                'digits_between:0,10',
            ],
            'aggregate' => [
                'digits_between:0,10',
            ],
            'venue'     => [
                'digits_between:0,10',
            ],
            'referee'   => [
                'digits_between:0,10',
            ],
        ];
    }
}
