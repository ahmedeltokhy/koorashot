<?php

namespace App\Http\Requests;

use App\Continent;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyContinentRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('continent_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:continents,id',
        ];
    }
}
