<?php

namespace App\Http\Requests;

use App\League;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateLeagueRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('league_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'sm' => [
                'digits_between:0,10',
            ],
        ];
    }
}
