<?php

namespace App\Http\Requests;

use App\Team;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateTeamRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('team_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'sm'             => [
                'digits_between:0,10',
            ],
            'founded'        => [
                'digits_between:0,10',
            ],
            'venue'          => [
                'digits_between:0,10',
            ],
            'current_season' => [
                'digits_between:0,10',
            ],
        ];
    }
}
