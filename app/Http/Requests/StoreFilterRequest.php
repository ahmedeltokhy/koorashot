<?php

namespace App\Http\Requests;

use App\Filter;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreFilterRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('filter_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'imageable_id'      => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'imageable_type' => [
                'required',
            ],
            'image'          => [
                'required',
            ],
        ];
    }
}
