<?php

namespace App\Http\Requests;

use App\Season;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateSeasonRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('season_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'sm'            => [
                'digits_between:0,10',
            ],
            'current_round' => [
                'digits_between:0,10',
            ],
            'current_stage' => [
                'digits_between:0,10',
            ],
        ];
    }
}
