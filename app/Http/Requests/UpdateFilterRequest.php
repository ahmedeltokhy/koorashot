<?php

namespace App\Http\Requests;

use App\Filter;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateFilterRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('filter_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'imageable'      => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'imageable_type' => [
                'required',
            ],
        ];
    }
}
