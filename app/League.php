<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class League extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    public $table = 'leagues';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'sm',
        'name',
        'active',
        'is_cup',
        'name_en',
        'logo_path',
        'updated_at',
        'created_at',
        'deleted_at',
        'country_id',
        'predictions',
        'current_round',
        'current_stage',
        'live_standings',
        'current_season',
        'topscorer_cards',
        'topscorer_goals',
        'topscorer_assists',
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }
    public function news()
    {
        return $this->hasMany(News::class);
    }

    public function getLogoAttribute()
    {
        $file = $this->getMedia('logo')->last();

        if ($file) {
            $file->url = $file->getUrl();
        }

        return $file;
    }
    public function filters()
    {
      return $this->morphMany('App\Filter', 'imageable');
    }
}
