<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Season extends Model
{
    use SoftDeletes;

    public $table = 'seasons';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'sm',
        'name',
        'league_id',
        'created_at',
        'updated_at',
        'deleted_at',
        'current_round',
        'current_stage',
        'is_current_season',
    ];

    public function league()
    {
        return $this->belongsTo(League::class, 'league_id');
    }
}
