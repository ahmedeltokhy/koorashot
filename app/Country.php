<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    use SoftDeletes;

    public $table = 'countries';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'sm',
        'lat',
        'iso',
        'lng',
        'name',
        'fifa',
        'flag',
        'iso_2',
        'name_en',
        'sub_region',
        'created_at',
        'updated_at',
        'deleted_at',
        'continent_id',
        'world_region',
    ];

    public function continent()
    {
        return $this->belongsTo(Continent::class, 'continent_id');
    }
}
