<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class News extends Model
{
    

    public $table = 'news';

    protected $dates = [
        'created_at',
        'updated_at',
       
    ];

   

    public function league()
    {
        return $this->belongsTo(League::class, 'league_id');
    }
    public function Team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }
    public function Source()
    {
        return $this->belongsTo(Source::class, 'source_id');
    }

   
}
