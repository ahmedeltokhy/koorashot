<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Match extends Model
{
    use SoftDeletes;

    public $table = 'matches';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'time',
        'date',
        'minutes_passed',
        'status',
        'group_name',
        'pitch',
        'venue',
        'match',
        'round_name',
        'stage',
        'scores',
        'colors',
        'coaches',
        'referee',
        'aggregate',
        'standings',
        'league_id',
        'season_id',
        'updated_at',
        'created_at',
        'deleted_at',
        'formations',
        'attendance',
        'commentaries',
        'local_team_id',
        'weather_report',
        'winner_team_id',
        'visitor_team_id',
        'winning_odds_calculated',
    ];
public function getByDate($date){
    return $this->where('date',$date)
    ->orderBy('date','asc')->orderBy('time','asc');
}
public function getByDateRange($startDate,$endDate){
    return $this->whereBetween('date',[$startDate,$endDate])
        ->orderBy('date','asc')->orderBy('time','asc');

}

public function getByDateRangeForUserFollowedTeams($startDate,$endDate,$followedTeamsIds){
    return $this->whereBetween('date',[$startDate,$endDate])->where(function($query) use($followedTeamsIds){
        $query->whereIn('visitor_team_id',$followedTeamsIds)
        ->orWhereIn('local_team_id',$followedTeamsIds);})
        ->orderBy('date','asc')->orderBy('time','asc');
}

//relations
    public function league()
    {
        return $this->belongsTo(League::class, 'league_id');
    }

    public function season()
    {
        return $this->belongsTo(Season::class, 'season_id');
    }

    public function local_team()
    {
        return $this->belongsTo(Team::class, 'local_team_id');
    }
    public function events(){
        return $this->hasMany(Event::class);
    }

    public function visitor_team()
    {
        return $this->belongsTo(Team::class, 'visitor_team_id');
    }

    public function winner_team()
    {
        return $this->belongsTo(Team::class, 'winner_team_id');
    }
}
