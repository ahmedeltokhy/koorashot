<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Event extends Model
{
   

    public $table = 'match_events';

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

  

    public function match()
    {
        return $this->belongsTo(Match::class, 'match_id');
    }
    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }

    
}
