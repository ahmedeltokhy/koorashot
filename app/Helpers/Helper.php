<?php


namespace App;


use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

if (!function_exists('validate_Api_request')) {
    function validate_Api_request($request, $roles)
    {
        $validation = Validator::make($request, $roles);
        if ($validation->fails()) {
            $errors = json_decode($validation->errors());
            $errors_string = '';
            foreach ($errors as $error) {
                $errors_string .= $error[0] . "\n<br>";
            }
            return J_response(Response::HTTP_EXPECTATION_FAILED, $errors_string, null);
        }
        return J_response(Response::HTTP_OK, '', null);
    }

    function J_response($status, $message, $data)
    {
        $result_array['status'] = $status;
        $result_array['message'] = $message;
        $result_array['data'] = $data;
        return $result_array;
    }

}

if(!function_exists('send_email')){
     function send_email($to, $message, $subject, $attach = null, $file_name = '')
    {

        $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
        try {
            //Server Settings
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->CharSet = 'UTF-8';
            $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'aaa.computerscience@gmail.com';                 // SMTP username
            $mail->Password = 'fmcycoqqskxdcngi';                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            //Recipients
            $mail->setFrom('no_replay@Koorashot.com', 'Koorashot App.');
            foreach ($to as $item) {
                $mail->addAddress($item, 'KooraShot');     // Add a recipient
            }
            $body = $message;
            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body = $body;

            //add attachment
            if ($attach != null) {
                $mail->addAttachment($attach, $file_name);
            }

            return $mail->send();

        } catch (Exception $e) {
//                echo 'Message could not be sent.';
            return 'Mailer Error: ' . $mail->ErrorInfo;
        }


    }

}