<?php

namespace App\Console\Commands;
use App\Traits\MatchTrait;
use Illuminate\Console\Command;

class LiveMatches extends Command
{
    use MatchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'live:matches';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Push FCM Notifcation In Case Of Goal Event';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->getInPlayMatchesFromSupplier();
    }
}
