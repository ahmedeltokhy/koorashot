<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Source;
use App\News;
class RssFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rss:feed {source=all}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Updated News From Its Source';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if($this->argument('source')=='all'){
            $sources=Source::all();
            foreach($sources as $source){
               $this->getSourceNews($source);
            }
        }else{
            $source=Source::where('name_en','like',$this->argument('source'))->first();
            $this->getSourceNews($source);
        }
    }

    function getSourceNews($source){
        if($source){
            if($source->rss_url){
                $client = new \GuzzleHttp\Client();
                $response = $client->request('GET', $source->rss_url);
                $xml= simplexml_load_string($response->getBody()) ;
                $json = json_encode($xml);
                $array= json_decode($json,TRUE);
                foreach($array['channel']['item'] as $item){
                    //check if title exists
                    $is_exist=News::where('title',$item['title'])->first();
                    if($is_exist){
                        continue;
                    }else{
                        $news=new News;
                        $news->source_id=$source->id;
                        $news->title=$item['title'];
                        $news->body=(gettype($item['description'])=='array')
                            ?implode($item['description']):$item['description'];
                        $news->link=$item['link']??$item['guid'];
                        $news->image_path=$item['image'] ?? null;
                        $news->publish_date=$item['pubDate'] ?? null;
                        $news->save();
                    }
                }
            }
         echo "Updating news ....";
        }else{
           echo 'Source Not Found';
        }
    }
   
}
