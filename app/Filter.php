<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Filter extends Model implements HasMedia
{
    use HasMediaTrait;

    public $table = 'filters';

    protected $appends = [
        'image',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
       
    ];

    protected $fillable = [
        'url',
        'imageable_id',
        'created_at',
        'updated_at',
       'imageable_type',
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function getImageAttribute()
    {
        $file = $this->getMedia('image')->last();

        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
        }

        return $file;
    }
    
    public function imageable()
    {
        return $this->morphTo();
    }
}
