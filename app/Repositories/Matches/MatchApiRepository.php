<?php

namespace App\Repositories\Matches;
use App\Traits\MatchTrait;
use App\Match;
use Illuminate\Cache\CacheManager;

class MatchApiRepository implements MatchRepositoryInterface
{
    use MatchTrait;

  

    public function getByDate($date){
        return $this->getMatchesFromSupplier($date);
    }

    public function getByDateRange($startDate,$endDate){}

    public function getByDateRangeForUserFollowedTeams($startDate,$endDate,$user){}
    

	public function find(int $id) {
         $this->getMatchFromSupplierById($id);
         
    }
}