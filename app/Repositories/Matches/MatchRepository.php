<?php

namespace App\Repositories\Matches;

use App\Match;

class MatchRepository implements MatchRepositoryInterface
{
	protected $model;

	public function __construct(Match $model) {
		$this->model = $model;
	}

	public function find(int $id) {
		return $this->model->find($id);
    }
	
	 public function getByDate($date){
        return $this->model->getByDate($date);
	}
	public function getByDateRange($startDate,$endDate){
		return $this->model->getByDateRange($startDate,$endDate);
	}

	public function getByDateRangeForUserFollowedTeams($startDate,$endDate,$followedTeamsIds){

		return $this->model->getByDateRangeForUserFollowedTeams($startDate,$endDate,$followedTeamsIds);
	}



	

    

}