<?php

namespace App\Repositories\Matches;
use Illuminate\Support\Facades\Cache;
use App\Match;
use Illuminate\Cache\CacheManager;
//traits
use App\Traits\DateTrait;

class MatchCacheRepository implements MatchRepositoryInterface
{
	use DateTrait;

	protected $repo;
	protected $apiRepo;
	protected $cache;

    public function __construct(CacheManager $cache, MatchRepository $repo,MatchApiRepository $apiRepo) {
		$this->repo = $repo;
		$this->apiRepo=$apiRepo;
		$this->cache = $cache;
	}

	
	
	public function getByDateRangeForUserFollowedTeams($startDate,$endDate,$followedTeamsIds){

		$this->getByDateRange($startDate,$endDate);
		return $this->repo->getByDateRangeForUserFollowedTeams($startDate,$endDate,$followedTeamsIds)->get();
	}

	public function getByDateRange($startDate,$endDate){
          
        $incrementalDate=$startDate;
		do{
			$this->getByDate($incrementalDate);
			$incrementalDate=$this->addDay($incrementalDate);
		}while($incrementalDate<=$endDate);
		return $this->repo->getByDateRange($startDate,$endDate)->get();
	}

    public function getByDate($date){
		$dateStatus=$this->checkDateStatus($date);
		if($dateStatus==-1){  #this previous date
			return $this->cache->rememberForever('matches_at_'.$date,function()use($date) {
				$this->apiRepo->getByDate($date);
				return $this->repo->getByDate($date)->get();
			});	
		}elseif($dateStatus==0){  #today
			$this->apiRepo->getByDate($date);
			return $this->repo->getByDate($date)->get();
		}elseif($dateStatus==1){ #comming date 
			$minutesToMatchStart=$this->calcMinutesToMatchStart($date);
			return $this->cache->remember('matches_at_'.$date,$minutesToMatchStart,function()use($date) {
				if(count($this->repo->getByDate($date)->get())==0){
					$this->apiRepo->getByDate($date);
				}
				return $this->repo->getByDate($date)->get();
			});
		}
	}
	
	public function find(int $id) {
		$key='match_'.$id;	
		if ($this->cache->has($key)) {
			$match=$this->cache->pull($key);
		}else{
			$match=$this->repo->find($id);
		}
		$dateStatus=$this->checkDateStatus($match->date);
		//finished and status updated so :- cache forever ->first cache
		if($dateStatus==-1 &&($match->status=='FT'||$match->status=='AET'||$match->status=='FT_PEN'||$match->status=='CANCL')){ 
			return $this->cache->rememberForever($key,function ()use($id) {
				$this->apiRepo->find($id); 
				return $this->repo->find($id);
			});
		}elseif($dateStatus==1 &&($match->status=='NS'||$match->status=='POSTP'||$match->status=='TBA')){   #comming so:- cheche to matche time ->second cache
			$minutesToMatchStart=$this->calcMinutesToMatchStart($match->date,$match->time);
			return $this->cache->remember('match_'.$id,$minutesToMatchStart,function()use($id) {
				return $this->repo->find($id);
			});
		}else{  
			 $this->apiRepo->find($id);  # get from supplier api && insert into 
			 return $this->repo->find($id);
		}
	}
}