<?php

namespace App\Repositories\Matches;

interface MatchRepositoryInterface 
{
    
    public function find(int $id);

    public function getByDate($date);

    public function getByDateRange($startDate,$endDate);

    public function getByDateRangeForUserFollowedTeams($startDate,$endDate,$followedTeamsIds);



	

}