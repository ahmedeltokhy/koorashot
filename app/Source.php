<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Source extends Model
{
    

    public $table = 'sources';

    protected $dates = [
        'created_at',
        'updated_at',
       
    ];

    protected $fillable = [
        'name_en',
        'name',
        
    ];

    public function news()
    {
        return $this->hasMany(News::class);
    }
    public function users()
    {
        return $this->belongsToMany(User::class);
    }
    

   
}
