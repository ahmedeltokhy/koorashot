<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Model
{
    use SoftDeletes;

    public $table = 'teams';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'sm',
        'name',
        'logo',
        'venue',
        'name_en',
        'twitter',
        'founded',
        'short_code',
        'country_id',
        'created_at',
        'updated_at',
        'deleted_at',
        'national_team',
        'current_season',
    ];

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }
    public function users()
    {
        return $this->belongsToMany(User::class);
    }
    public function filters()
    {
        return $this->morphMany('App\Filter', 'imageable');
    }
    public function news()
    {
        return $this->hasMany(News::class);
    }
    public function season()
    {
        return $this->belongsTo(Season::class,'current_season');
    }
}
