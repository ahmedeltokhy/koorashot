<?php

namespace App\Traits;
use App\Match;
use Illuminate\Support\Carbon;
trait DateTrait{

    function addDay($date){
        
        $newDate=new Carbon($date);
        return $newDate->addDays(1)->format('Y-m-d');
    }

    function checkDateStatus($matchDate){
        $today=Carbon::now()->format('Y-m-d');
        $matchDate=new Carbon($matchDate);
        $matchDate =$matchDate->format('Y-m-d');
        if($matchDate<$today){
            return -1;
        }elseif($matchDate>$today){
            return 1;
        }else{
            return 0;
        }
    }
    function isToday($date){
        $today=Carbon::now()->format('Y-m-d');
        $date=new Carbon($date);
        $date =$date->format('Y-m-d');
        if($date ==$today){
            return true;
        }
        return false;
    }

    function dateRange($date,$page=0){
        $startDate=new Carbon($date);
        $endDate=new Carbon($date); 
        $date=$startDate->format('Y-m-d');
        $endDays=$page*7;
        if($page>0){
            $startDays=($page-1)*7;
            list($start_date,$end_date)=[$startDate->addDays($startDays)->format('Y-m-d'),$endDate->addDays($endDays-1)->format('Y-m-d')];
        }elseif($page<0){
            $startDays=($page+1)*7;
            list($start_date,$end_date)=[$startDate->addDays($endDays+1)->format('Y-m-d'),$endDate->addDays($startDays)->format('Y-m-d')];
        }else{
            list($start_date,$end_date)=[$date,$date];
        }
        return ['start_date'=>$start_date,'end_date'=>$end_date];
    }

    function setDataArray($start_date,$end_date){
        $data=[]; 
        while($start_date<=$end_date){
        $data_object=['date'=>$start_date,'matches'=>[]];
        array_push($data,$data_object);
        $start_date=$this->addDay($start_date);
        }
       return $data;
    }

    function dateRangeForMatchesCount($date,$matches_count,$days){
        $newDate=new Carbon($date); 
        $date=$newDate->format('Y-m-d');
        if($matches_count>1){
            list($start_date,$end_date)=[$date,$newDate->addDays($days-1)->format('Y-m-d')];
        }elseif($matches_count<-1){
            list($start_date,$end_date)=[$newDate->addDays(gmp_neg($days)+1)->format('Y-m-d'),$date];
        }
        dd(['start_date'=>$start_date,'end_date'=>$end_date]);
        return ['start_date'=>$start_date,'end_date'=>$end_date];
    }

    function calcMinutesToMatchStart($date,$time='00:00:00'){
           return Carbon::now()->diffInMinutes(new Carbon($date.' '.$time));
    }

}