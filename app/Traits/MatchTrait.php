<?php

namespace App\Traits;
use App\Match;
use App\Event;
use Exception;
use App\Traits\FCMTrait;
use App\Traits\DateTrait;
use Illuminate\Support\Carbon;
trait MatchTrait{
    use FCMTrait,DateTrait;
      

    public function getMatchesFromSupplier($date){
          
        $matches_api_base_url=$this->isToday($date)?'https://soccer.sportmonks.com/api/v2.0/livescores'
                             :'https://soccer.sportmonks.com/api/v2.0/fixtures/date/'.$date;
        $includes='round,group,referee';
        $current_page=1;$count_pages=1;  // in case of pagination
        $apiToken='6780XK6x95dDUxH71gYeQaAzfz0g34hr1S9mSh8H0CYRyhirxW8FNUiA3MwS';
        $matches_api_query_url='?api_token='.$apiToken.'&include='.$includes.'&page='.$current_page;
        $client = new \GuzzleHttp\Client();
        $url=$matches_api_base_url.$matches_api_query_url;
        // dd($url);s     
        while($count_pages>=$current_page){
            $response = $client->request('GET', $url);
            $matches= json_decode($response->getBody(),true);
            $count_pages=$matches["meta"]["pagination"]["total_pages"];
            foreach($matches["data"] as $match){
             
                $this->insertIntoMatchesTable($match);
           }
            $current_page++;
        }
      return true;
   }

   function getInPlayMatchesFromSupplier(){
    $matches_api_base_url='https://soccer.sportmonks.com/api/v2.0/livescores/now/';
    $includes='events';
    $apiToken='6780XK6x95dDUxH71gYeQaAzfz0g34hr1S9mSh8H0CYRyhirxW8FNUiA3MwS';
    $matches_api_query_url='?api_token='.$apiToken.'&include='.$includes;
    $client = new \GuzzleHttp\Client();
    $url=$matches_api_base_url.$matches_api_query_url;        
    $response = $client->request('GET', $url);
    $matches= json_decode($response->getBody(),true);
    foreach($matches["data"] as $match){
        $this->insertIntoMatchesTable($match);
        if(isset($match['events'])){
            foreach($match['events']['data'] as $event){
                $this->insertIntoMatchEventsTable($event);
            }
        }
    }
    return true;   
}
//get match by id from supplier and record it
function getMatchFromSupplierById($id){
    $client = new \GuzzleHttp\Client();
    $apiToken='6780XK6x95dDUxH71gYeQaAzfz0g34hr1S9mSh8H0CYRyhirxW8FNUiA3MwS';
    $response = $client->request('GET', 
     'https://soccer.sportmonks.com/api/v2.0/fixtures/'.$id.'?api_token='.$apiToken.'&include=events,round,group,referee');
    $match= json_decode($response->getBody(),true);
    $this->insertIntoMatchesTable($match['data']);
    if(isset($match['data']['events'])){
        foreach($match['data']['events']['data'] as $event){
            $this->insertIntoMatchEventsTable($event);
        }
    }
}
    
//insertion part  


   
  //get user's followed teams 
    function getUserFollowedTeamsIds($user){
        $followed_teams_ids=[];
        foreach($user->teams as $team){
            if($team->pivot->timeline){
               array_push($followed_teams_ids,$team->id);
            }
        }
       return $followed_teams_ids;
    }
   

     
  //recordein in event tables
  function insertIntoMatchEventsTable($event){
    $is_exist=Event::where('id',$event["id"])->first();
    if($is_exist){
       return;
    }else{
        $the_event=new Event;
        $the_event->id= $event["id"];
        $the_event->result=$event['result'];
        $the_event->type=$event['type'];
        $the_event->player_id=$event['player_id']??null;
        $the_event->player_name=$event['player_name']??null;
        $the_event->related_player_id=$event['related_player_id']??null;
        $the_event->related_player_name=$event['related_player_name'];
        $the_event->team_id=$event['team_id'];
        $the_event->minute=$event['minute'];
        $the_event->match_id=$event['fixture_id'];
        try{
            $the_event->save();
            if($the_event->type=='goal'||($the_event->type=='penalty' &&!is_null($the_event->result))){
                $this->sendMessageThroughFCM($the_event->team_id,'goal');
            }
        }catch(Exception $e){
            return false;
        }
    }
   
}


 //matchhes table
 function insertIntoMatchesTable($match){
    // insert inot data dase
    $is_exist=Match::where('id',$match["id"])->first();
    if($is_exist){
        return $this->updateMatch($is_exist,$match);
    }
    $the_match=new Match;
    $the_match->id= $match["id"];
    $the_match->round_name=$match["round_id"]?$match["round"]["data"]['name']:null;
    $the_match->group_name=$match['group_id']?(gettype($match['group_id'])=='array')?$match['group_id']['data']['name']:null:null;
    $the_match->referee=$match['referee_id']? $match['referee']['data']['common_name']:null;
    $the_match->localteam_score=$match["scores"]['localteam_score'];
    $the_match->visitorteam_score=$match["scores"]['visitorteam_score'];
    $the_match->time=$match['time']['starting_at']['time'];
    $the_match->minutes_passed=$match['time']['minute'];
    $the_match->date=$match['time']['starting_at']['date'];
    $the_match->status=$match['time']['status'];
    $the_match->league_id=$match['league_id'];
    $the_match->season_id=$match['season_id'];
    $the_match->local_team_id=$match['localteam_id'];
    $the_match->visitor_team_id=$match['visitorteam_id'];
    $the_match->winner_team_id=$match['winner_team_id'];
    try{
        $the_match->save();
    }catch(Exception $e){
        return false;// $e->getMessage();
        /*Insufficient privileges, Season with id 16387 does not exists or is not accessible from your plan.
        ** this season not exist 
        **and team with id=132590 doesn't exist 
        **so if match for this team will cause exception in database =>team_id is forign key
        ** if exception will skip this record
        */
    }
}
function updateMatch($the_match,$match){
   
    $the_match->localteam_score=$match["scores"]['localteam_score'];
    $the_match->visitorteam_score=$match["scores"]['visitorteam_score'];
    $the_match->time=$match['time']['starting_at']['time'];
    $the_match->minutes_passed=$match['time']['minute'];
    $the_match->date=$match['time']['starting_at']['date'];
    $the_match->status=$match['time']['status'];
    $the_match->winner_team_id=$match['winner_team_id'];
    try{
        $the_match->save();
    }catch(Exception $e){
        return false;
    }

}



}