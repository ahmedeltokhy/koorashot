@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.filter.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.filters.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <input class="form-control {{ $errors->has('imageable') ? 'is-invalid' : '' }}" type="hidden" name="imageable_id" id="imageable_id" value="{{ old('imageable') }}" step="1" required>
                @if($errors->has(''))
                    <span class="text-danger">{{ $errors->first('') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.filter.fields.imageable_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="imageable_type">{{ trans('cruds.filter.fields.imageable_type') }}</label>
                {{-- <input class="form-control {{ $errors->has('imageable_type') ? 'is-invalid' : '' }}" type="text"  value="{{ old('imageable_type', '') }}" required> --}}
                <select name="imageable_type" id="imageable_type" class="form-control select2">
                    <option value="">يرجى الإختيار</option>
                    <option value="App\Team" >Team</option>
                    <option value="App\League" >League</option>
                </select>
                @if($errors->has(''))
                    <span class="text-danger">{{ $errors->first('') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.filter.fields.imageable_type_helper') }}</span>
            </div>
            {{-- if type league --}}
            <div class="form-group {{ $errors->has('imageable_id') ? 'has-error' : '' }}" id="league-div" >
                <label for="league">league</label>
                <select  id="league" class="form-control select2">
                    @foreach($leagues as $id => $league)
                        <option value="{{ $id }}" {{ (isset($filter) && $filter->league ? $filter->league->id : old('imageable_id')) == $id ? 'selected' : '' }}>{{ $league }}</option>
                    @endforeach
                </select>
                @if($errors->has('imageable_id'))
                    <p class="help-block">
                        {{ $errors->first('imageable_id') }}
                    </p>
                @endif
            </div>
            {{--  --}}
            {{-- if type team --}}
            <div class="form-group {{ $errors->has('imageable_id') ? 'has-error' : '' }}" id="team-div" >
                <label for="team">team</label>
                <select  id="team" class="form-control select2">
                    @foreach($teams as $id => $team)
                        <option value="{{ $id }}" {{ (isset($filter) && $filter->team ? $filter->team->id : old('imageable_id')) == $id ? 'selected' : '' }}>{{ $team }}</option>
                    @endforeach
                </select>
                @if($errors->has('imageable_id'))
                    <p class="help-block">
                        {{ $errors->first('imageable_id') }}
                    </p>
                @endif
            </div>
            {{--  --}}
            <div class="form-group">
                <label class="required" for="image">{{ trans('cruds.filter.fields.image') }}</label>
                {{-- <div class="needsclick dropzone {{ $errors->has('image') ? 'is-invalid' : '' }}" id="image-dropzone">
                </div> --}}
                <input name="image" type="file" id="image">
                @if($errors->has(''))
                    <span class="text-danger">{{ $errors->first('') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.filter.fields.image_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    Dropzone.options.imageDropzone = {
    url: '{{ route('admin.filters.storeMedia') }}',
    maxFilesize: 2, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 2,
      width: 4096,
      height: 4096
    },
    success: function (file, response) {
      $('form').find('input[name="image"]').remove()
      $('form').append('<input type="hidden" name="image" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="image"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($filter) && $filter->image)
      var file = {!! json_encode($filter->image) !!}
          this.options.addedfile.call(this, file)
      this.options.thumbnail.call(this, file, '{{ $filter->image->getUrl('thumb') }}')
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="image" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
    error: function (file, response) {
        if ($.type(response) === 'string') {
            var message = response //dropzone sends it's own error messages in string
        } else {
            var message = response.errors.file
        }
        file.previewElement.classList.add('dz-error')
        _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
        _results = []
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i]
            _results.push(node.textContent = message)
        }

        return _results
    }
}
// 
$('#team-div').hide();
$('#league-div').hide();
$('#imageable_type').change(function(){
    alert($(this).val());
    if($(this).val()=='App\\Team'){
        $('#team-div').show();
        $('#league-div').hide();
    }else if($(this).val()=='App\\League'){
        $('#league-div').show();
        $('#team-div').hide();
    }else{
        $('#team-div').hide();
        $('#league-div').hide();

    }
   
});

  $('#league').change(function(){
    
    $('#imageable_id').val($(this).val());
    
  });
  $('#team').change(function(){
    $('#imageable_id').val($(this).val());
  });
  
</script>
@endsection