@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.team.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.teams.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('sm') ? 'has-error' : '' }}">
                <label for="sm">{{ trans('cruds.team.fields.sm') }}</label>
                <input type="number" id="sm" name="sm" class="form-control" value="{{ old('sm', isset($team) ? $team->sm : '') }}" step="1">
                @if($errors->has('sm'))
                    <p class="help-block">
                        {{ $errors->first('sm') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.team.fields.sm_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">{{ trans('cruds.team.fields.name') }}</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($team) ? $team->name : '') }}">
                @if($errors->has('name'))
                    <p class="help-block">
                        {{ $errors->first('name') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.team.fields.name_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('name_en') ? 'has-error' : '' }}">
                <label for="name_en">{{ trans('cruds.team.fields.name_en') }}</label>
                <input type="text" id="name_en" name="name_en" class="form-control" value="{{ old('name_en', isset($team) ? $team->name_en : '') }}">
                @if($errors->has('name_en'))
                    <p class="help-block">
                        {{ $errors->first('name_en') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.team.fields.name_en_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('short_code') ? 'has-error' : '' }}">
                <label for="short_code">{{ trans('cruds.team.fields.short_code') }}</label>
                <input type="text" id="short_code" name="short_code" class="form-control" value="{{ old('short_code', isset($team) ? $team->short_code : '') }}">
                @if($errors->has('short_code'))
                    <p class="help-block">
                        {{ $errors->first('short_code') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.team.fields.short_code_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('twitter') ? 'has-error' : '' }}">
                <label for="twitter">{{ trans('cruds.team.fields.twitter') }}</label>
                <input type="text" id="twitter" name="twitter" class="form-control" value="{{ old('twitter', isset($team) ? $team->twitter : '') }}">
                @if($errors->has('twitter'))
                    <p class="help-block">
                        {{ $errors->first('twitter') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.team.fields.twitter_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('country_id') ? 'has-error' : '' }}">
                <label for="country">{{ trans('cruds.team.fields.country') }}</label>
                <select name="country_id" id="country" class="form-control select2">
                    @foreach($countries as $id => $country)
                        <option value="{{ $id }}" {{ (isset($team) && $team->country ? $team->country->id : old('country_id')) == $id ? 'selected' : '' }}>{{ $country }}</option>
                    @endforeach
                </select>
                @if($errors->has('country_id'))
                    <p class="help-block">
                        {{ $errors->first('country_id') }}
                    </p>
                @endif
            </div>
            <div class="form-group {{ $errors->has('national_team') ? 'has-error' : '' }}">
                <label for="national_team">{{ trans('cruds.team.fields.national_team') }}</label>
                <input name="national_team" type="hidden" value="0">
                <input value="1" type="checkbox" id="national_team" name="national_team" {{ old('national_team', 0) == 1 ? 'checked' : '' }}>
                @if($errors->has('national_team'))
                    <p class="help-block">
                        {{ $errors->first('national_team') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.team.fields.national_team_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('founded') ? 'has-error' : '' }}">
                <label for="founded">{{ trans('cruds.team.fields.founded') }}</label>
                <input type="number" id="founded" name="founded" class="form-control" value="{{ old('founded', isset($team) ? $team->founded : '') }}" step="1">
                @if($errors->has('founded'))
                    <p class="help-block">
                        {{ $errors->first('founded') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.team.fields.founded_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('logo') ? 'has-error' : '' }}">
                <label for="logo">{{ trans('cruds.team.fields.logo') }}</label>
                <input type="text" id="logo" name="logo" class="form-control" value="{{ old('logo', isset($team) ? $team->logo : '') }}">
                @if($errors->has('logo'))
                    <p class="help-block">
                        {{ $errors->first('logo') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.team.fields.logo_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('venue') ? 'has-error' : '' }}">
                <label for="venue">{{ trans('cruds.team.fields.venue') }}</label>
                <input type="number" id="venue" name="venue" class="form-control" value="{{ old('venue', isset($team) ? $team->venue : '') }}" step="1">
                @if($errors->has('venue'))
                    <p class="help-block">
                        {{ $errors->first('venue') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.team.fields.venue_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('current_season') ? 'has-error' : '' }}">
                <label for="current_season">{{ trans('cruds.team.fields.current_season') }}</label>
                <input type="number" id="current_season" name="current_season" class="form-control" value="{{ old('current_season', isset($team) ? $team->current_season : '') }}" step="1">
                @if($errors->has('current_season'))
                    <p class="help-block">
                        {{ $errors->first('current_season') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.team.fields.current_season_helper') }}
                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>
@endsection