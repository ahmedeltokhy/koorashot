@extends('layouts.admin')
@section('content')
@can('country_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.countries.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.country.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.country.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.country.fields.name') }}
                        </th>
                        <th>
                            {{ trans('cruds.country.fields.name_en') }}
                        </th>
                        <th>
                            {{ trans('cruds.country.fields.continent') }}
                        </th>
                        <th>
                            {{ trans('cruds.country.fields.sub_region') }}
                        </th>
                        <th>
                            {{ trans('cruds.country.fields.world_region') }}
                        </th>
                        <th>
                            {{ trans('cruds.country.fields.fifa') }}
                        </th>
                        <th>
                            {{ trans('cruds.country.fields.iso') }}
                        </th>
                        <th>
                            {{ trans('cruds.country.fields.iso_2') }}
                        </th>
                        <th>
                            {{ trans('cruds.country.fields.lat') }}
                        </th>
                        <th>
                            {{ trans('cruds.country.fields.lng') }}
                        </th>
                        <th>
                            {{ trans('cruds.country.fields.flag') }}
                        </th>
                        <th>
                            {{ trans('cruds.country.fields.sm') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($countries as $key => $country)
                        <tr data-entry-id="{{ $country->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $country->name ?? '' }}
                            </td>
                            <td>
                                {{ $country->name_en ?? '' }}
                            </td>
                            <td>
                                {{ $country->continent->name ?? '' }}
                            </td>
                            <td>
                                {{ $country->sub_region ?? '' }}
                            </td>
                            <td>
                                {{ $country->world_region ?? '' }}
                            </td>
                            <td>
                                {{ $country->fifa ?? '' }}
                            </td>
                            <td>
                                {{ $country->iso ?? '' }}
                            </td>
                            <td>
                                {{ $country->iso_2 ?? '' }}
                            </td>
                            <td>
                                {{ $country->lat ?? '' }}
                            </td>
                            <td>
                                {{ $country->lng ?? '' }}
                            </td>
                            <td>
                                {{ $country->flag ?? '' }}
                            </td>
                            <td>
                                {{ $country->sm ?? '' }}
                            </td>
                            <td>
                                @can('country_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.countries.show', $country->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan
                                @can('country_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.countries.edit', $country->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
                                @can('country_delete')
                                    <form action="{{ route('admin.countries.destroy', $country->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.countries.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
@can('country_delete')
  dtButtons.push(deleteButton)
@endcan

  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection