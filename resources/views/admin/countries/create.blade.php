@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.country.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.countries.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">{{ trans('cruds.country.fields.name') }}</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($country) ? $country->name : '') }}">
                @if($errors->has('name'))
                    <p class="help-block">
                        {{ $errors->first('name') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.country.fields.name_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('name_en') ? 'has-error' : '' }}">
                <label for="name_en">{{ trans('cruds.country.fields.name_en') }}</label>
                <input type="text" id="name_en" name="name_en" class="form-control" value="{{ old('name_en', isset($country) ? $country->name_en : '') }}">
                @if($errors->has('name_en'))
                    <p class="help-block">
                        {{ $errors->first('name_en') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.country.fields.name_en_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('continent_id') ? 'has-error' : '' }}">
                <label for="continent">{{ trans('cruds.country.fields.continent') }}</label>
                <select name="continent_id" id="continent" class="form-control select2">
                    @foreach($continents as $id => $continent)
                        <option value="{{ $id }}" {{ (isset($country) && $country->continent ? $country->continent->id : old('continent_id')) == $id ? 'selected' : '' }}>{{ $continent }}</option>
                    @endforeach
                </select>
                @if($errors->has('continent_id'))
                    <p class="help-block">
                        {{ $errors->first('continent_id') }}
                    </p>
                @endif
            </div>
            <div class="form-group {{ $errors->has('sub_region') ? 'has-error' : '' }}">
                <label for="sub_region">{{ trans('cruds.country.fields.sub_region') }}</label>
                <input type="text" id="sub_region" name="sub_region" class="form-control" value="{{ old('sub_region', isset($country) ? $country->sub_region : '') }}">
                @if($errors->has('sub_region'))
                    <p class="help-block">
                        {{ $errors->first('sub_region') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.country.fields.sub_region_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('world_region') ? 'has-error' : '' }}">
                <label for="world_region">{{ trans('cruds.country.fields.world_region') }}</label>
                <input type="text" id="world_region" name="world_region" class="form-control" value="{{ old('world_region', isset($country) ? $country->world_region : '') }}">
                @if($errors->has('world_region'))
                    <p class="help-block">
                        {{ $errors->first('world_region') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.country.fields.world_region_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('fifa') ? 'has-error' : '' }}">
                <label for="fifa">{{ trans('cruds.country.fields.fifa') }}</label>
                <input type="text" id="fifa" name="fifa" class="form-control" value="{{ old('fifa', isset($country) ? $country->fifa : '') }}">
                @if($errors->has('fifa'))
                    <p class="help-block">
                        {{ $errors->first('fifa') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.country.fields.fifa_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('iso') ? 'has-error' : '' }}">
                <label for="iso">{{ trans('cruds.country.fields.iso') }}</label>
                <input type="text" id="iso" name="iso" class="form-control" value="{{ old('iso', isset($country) ? $country->iso : '') }}">
                @if($errors->has('iso'))
                    <p class="help-block">
                        {{ $errors->first('iso') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.country.fields.iso_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('iso_2') ? 'has-error' : '' }}">
                <label for="iso_2">{{ trans('cruds.country.fields.iso_2') }}</label>
                <input type="text" id="iso_2" name="iso_2" class="form-control" value="{{ old('iso_2', isset($country) ? $country->iso_2 : '') }}">
                @if($errors->has('iso_2'))
                    <p class="help-block">
                        {{ $errors->first('iso_2') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.country.fields.iso_2_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('lat') ? 'has-error' : '' }}">
                <label for="lat">{{ trans('cruds.country.fields.lat') }}</label>
                <input type="text" id="lat" name="lat" class="form-control" value="{{ old('lat', isset($country) ? $country->lat : '') }}">
                @if($errors->has('lat'))
                    <p class="help-block">
                        {{ $errors->first('lat') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.country.fields.lat_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('lng') ? 'has-error' : '' }}">
                <label for="lng">{{ trans('cruds.country.fields.lng') }}</label>
                <input type="text" id="lng" name="lng" class="form-control" value="{{ old('lng', isset($country) ? $country->lng : '') }}">
                @if($errors->has('lng'))
                    <p class="help-block">
                        {{ $errors->first('lng') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.country.fields.lng_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('sm') ? 'has-error' : '' }}">
                <label for="sm">{{ trans('cruds.country.fields.sm') }}</label>
                <input type="number" id="sm" name="sm" class="form-control" value="{{ old('sm', isset($country) ? $country->sm : '') }}" step="1">
                @if($errors->has('sm'))
                    <p class="help-block">
                        {{ $errors->first('sm') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.country.fields.sm_helper') }}
                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>
@endsection