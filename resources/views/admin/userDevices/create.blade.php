@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.userDevice.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.user-devices.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('user_id') ? 'has-error' : '' }}">
                <label for="user">{{ trans('cruds.userDevice.fields.user') }}</label>
                <select name="user_id" id="user" class="form-control select2">
                    @foreach($users as $id => $user)
                        <option value="{{ $id }}" {{ (isset($userDevice) && $userDevice->user ? $userDevice->user->id : old('user_id')) == $id ? 'selected' : '' }}>{{ $user }}</option>
                    @endforeach
                </select>
                @if($errors->has('user_id'))
                    <p class="help-block">
                        {{ $errors->first('user_id') }}
                    </p>
                @endif
            </div>
            <div class="form-group {{ $errors->has('token') ? 'has-error' : '' }}">
                <label for="token">{{ trans('cruds.userDevice.fields.token') }}</label>
                <input type="text" id="token" name="token" class="form-control" value="{{ old('token', isset($userDevice) ? $userDevice->token : '') }}">
                @if($errors->has('token'))
                    <p class="help-block">
                        {{ $errors->first('token') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.userDevice.fields.token_helper') }}
                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>
@endsection