@extends('layouts.admin')
@section('content')
@can('season_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.seasons.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.season.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.season.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.season.fields.sm') }}
                        </th>
                        <th>
                            {{ trans('cruds.season.fields.name') }}
                        </th>
                        <th>
                            {{ trans('cruds.season.fields.league') }}
                        </th>
                        <th>
                            {{ trans('cruds.season.fields.is_current_season') }}
                        </th>
                        <th>
                            {{ trans('cruds.season.fields.current_round') }}
                        </th>
                        <th>
                            {{ trans('cruds.season.fields.current_stage') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($seasons as $key => $season)
                        <tr data-entry-id="{{ $season->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $season->sm ?? '' }}
                            </td>
                            <td>
                                {{ $season->name ?? '' }}
                            </td>
                            <td>
                                {{ $season->league->sm ?? '' }}
                            </td>
                            <td>
                                {{ $season->is_current_season ? trans('global.yes') : trans('global.no') }}
                            </td>
                            <td>
                                {{ $season->current_round ?? '' }}
                            </td>
                            <td>
                                {{ $season->current_stage ?? '' }}
                            </td>
                            <td>
                                @can('season_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.seasons.show', $season->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan
                                @can('season_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.seasons.edit', $season->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
                                @can('season_delete')
                                    <form action="{{ route('admin.seasons.destroy', $season->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.seasons.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
@can('season_delete')
  dtButtons.push(deleteButton)
@endcan

  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection