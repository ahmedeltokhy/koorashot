@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.season.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.seasons.update", [$season->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group {{ $errors->has('sm') ? 'has-error' : '' }}">
                <label for="sm">{{ trans('cruds.season.fields.sm') }}</label>
                <input type="number" id="sm" name="sm" class="form-control" value="{{ old('sm', isset($season) ? $season->sm : '') }}" step="1">
                @if($errors->has('sm'))
                    <p class="help-block">
                        {{ $errors->first('sm') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.season.fields.sm_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">{{ trans('cruds.season.fields.name') }}</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($season) ? $season->name : '') }}">
                @if($errors->has('name'))
                    <p class="help-block">
                        {{ $errors->first('name') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.season.fields.name_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('league_id') ? 'has-error' : '' }}">
                <label for="league">{{ trans('cruds.season.fields.league') }}</label>
                <select name="league_id" id="league" class="form-control select2">
                    @foreach($leagues as $id => $league)
                        <option value="{{ $id }}" {{ (isset($season) && $season->league ? $season->league->id : old('league_id')) == $id ? 'selected' : '' }}>{{ $league }}</option>
                    @endforeach
                </select>
                @if($errors->has('league_id'))
                    <p class="help-block">
                        {{ $errors->first('league_id') }}
                    </p>
                @endif
            </div>
            <div class="form-group {{ $errors->has('is_current_season') ? 'has-error' : '' }}">
                <label for="is_current_season">{{ trans('cruds.season.fields.is_current_season') }}</label>
                <input name="is_current_season" type="hidden" value="0">
                <input value="1" type="checkbox" id="is_current_season" name="is_current_season" {{ (isset($season) && $season->is_current_season) || old('is_current_season', 0) === 1 ? 'checked' : '' }}>
                @if($errors->has('is_current_season'))
                    <p class="help-block">
                        {{ $errors->first('is_current_season') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.season.fields.is_current_season_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('current_round') ? 'has-error' : '' }}">
                <label for="current_round">{{ trans('cruds.season.fields.current_round') }}</label>
                <input type="number" id="current_round" name="current_round" class="form-control" value="{{ old('current_round', isset($season) ? $season->current_round : '') }}" step="1">
                @if($errors->has('current_round'))
                    <p class="help-block">
                        {{ $errors->first('current_round') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.season.fields.current_round_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('current_stage') ? 'has-error' : '' }}">
                <label for="current_stage">{{ trans('cruds.season.fields.current_stage') }}</label>
                <input type="number" id="current_stage" name="current_stage" class="form-control" value="{{ old('current_stage', isset($season) ? $season->current_stage : '') }}" step="1">
                @if($errors->has('current_stage'))
                    <p class="help-block">
                        {{ $errors->first('current_stage') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.season.fields.current_stage_helper') }}
                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>
@endsection