@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.season.title') }}
    </div>

    <div class="card-body">
        <div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.season.fields.id') }}
                        </th>
                        <td>
                            {{ $season->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.season.fields.sm') }}
                        </th>
                        <td>
                            {{ $season->sm }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.season.fields.name') }}
                        </th>
                        <td>
                            {{ $season->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.season.fields.league') }}
                        </th>
                        <td>
                            {{ $season->league->sm ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.season.fields.is_current_season') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled {{ $season->is_current_season ? "checked" : "" }}>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.season.fields.current_round') }}
                        </th>
                        <td>
                            {{ $season->current_round }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.season.fields.current_stage') }}
                        </th>
                        <td>
                            {{ $season->current_stage }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>
    </div>
</div>
@endsection