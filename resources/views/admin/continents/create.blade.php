@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.continent.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.continents.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('sm') ? 'has-error' : '' }}">
                <label for="sm">{{ trans('cruds.continent.fields.sm') }}</label>
                <input type="number" id="sm" name="sm" class="form-control" value="{{ old('sm', isset($continent) ? $continent->sm : '') }}" step="1">
                @if($errors->has('sm'))
                    <p class="help-block">
                        {{ $errors->first('sm') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.continent.fields.sm_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">{{ trans('cruds.continent.fields.name') }}</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($continent) ? $continent->name : '') }}">
                @if($errors->has('name'))
                    <p class="help-block">
                        {{ $errors->first('name') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.continent.fields.name_helper') }}
                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>
@endsection