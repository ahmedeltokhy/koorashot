@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.match.title') }}
    </div>

    <div class="card-body">
        <div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.id') }}
                        </th>
                        <td>
                            {{ $match->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.match') }}
                        </th>
                        <td>
                            {{ $match->match }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.league') }}
                        </th>
                        <td>
                            {{ $match->league->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.season') }}
                        </th>
                        <td>
                            {{ $match->season->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.stage') }}
                        </th>
                        <td>
                            {{ $match->stage }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.round') }}
                        </th>
                        <td>
                            {{ $match->round }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.group') }}
                        </th>
                        <td>
                            {{ $match->group }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.aggregate') }}
                        </th>
                        <td>
                            {{ $match->aggregate }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.venue') }}
                        </th>
                        <td>
                            {{ $match->venue }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.referee') }}
                        </th>
                        <td>
                            {{ $match->referee }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.local_team') }}
                        </th>
                        <td>
                            {{ $match->local_team->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.visitor_team') }}
                        </th>
                        <td>
                            {{ $match->visitor_team->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.winner_team') }}
                        </th>
                        <td>
                            {{ $match->winner_team->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.weather_report') }}
                        </th>
                        <td>
                            {{ $match->weather_report }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.commentaries') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled {{ $match->commentaries ? "checked" : "" }}>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.attendance') }}
                        </th>
                        <td>
                            {{ $match->attendance }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.pitch') }}
                        </th>
                        <td>
                            {{ $match->pitch }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.winning_odds_calculated') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled {{ $match->winning_odds_calculated ? "checked" : "" }}>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.formations') }}
                        </th>
                        <td>
                            {{ $match->formations }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.scores') }}
                        </th>
                        <td>
                            {{ $match->scores }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.time') }}
                        </th>
                        <td>
                            {{ $match->time }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.coaches') }}
                        </th>
                        <td>
                            {{ $match->coaches }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.standings') }}
                        </th>
                        <td>
                            {{ $match->standings }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.colors') }}
                        </th>
                        <td>
                            {{ $match->colors }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>
    </div>
</div>
@endsection