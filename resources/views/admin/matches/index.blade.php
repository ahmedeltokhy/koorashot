@extends('layouts.admin')
@section('content')
@can('match_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.matches.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.match.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.match.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.match.fields.match') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.league') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.season') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.stage') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.round') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.group') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.aggregate') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.venue') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.referee') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.local_team') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.visitor_team') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.winner_team') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.weather_report') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.commentaries') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.attendance') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.pitch') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.winning_odds_calculated') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.formations') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.scores') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.time') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.coaches') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.standings') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.colors') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($matches as $key => $match)
                        <tr data-entry-id="{{ $match->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $match->match ?? '' }}
                            </td>
                            <td>
                                {{ $match->league->name ?? '' }}
                            </td>
                            <td>
                                {{ $match->season->name ?? '' }}
                            </td>
                            <td>
                                {{ $match->stage ?? '' }}
                            </td>
                            <td>
                                {{ $match->round ?? '' }}
                            </td>
                            <td>
                                {{ $match->group ?? '' }}
                            </td>
                            <td>
                                {{ $match->aggregate ?? '' }}
                            </td>
                            <td>
                                {{ $match->venue ?? '' }}
                            </td>
                            <td>
                                {{ $match->referee ?? '' }}
                            </td>
                            <td>
                                {{ $match->local_team->name ?? '' }}
                            </td>
                            <td>
                                {{ $match->visitor_team->name ?? '' }}
                            </td>
                            <td>
                                {{ $match->winner_team->name ?? '' }}
                            </td>
                            <td>
                                {{ $match->weather_report ?? '' }}
                            </td>
                            <td>
                                {{ $match->commentaries ? trans('global.yes') : trans('global.no') }}
                            </td>
                            <td>
                                {{ $match->attendance ?? '' }}
                            </td>
                            <td>
                                {{ $match->pitch ?? '' }}
                            </td>
                            <td>
                                {{ $match->winning_odds_calculated ? trans('global.yes') : trans('global.no') }}
                            </td>
                            <td>
                                {{ $match->formations ?? '' }}
                            </td>
                            <td>
                                {{ $match->scores ?? '' }}
                            </td>
                            <td>
                                {{ $match->time ?? '' }}
                            </td>
                            <td>
                                {{ $match->coaches ?? '' }}
                            </td>
                            <td>
                                {{ $match->standings ?? '' }}
                            </td>
                            <td>
                                {{ $match->colors ?? '' }}
                            </td>
                            <td>
                                @can('match_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.matches.show', $match->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan
                                @can('match_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.matches.edit', $match->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
                                @can('match_delete')
                                    <form action="{{ route('admin.matches.destroy', $match->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.matches.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
@can('match_delete')
  dtButtons.push(deleteButton)
@endcan

  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection