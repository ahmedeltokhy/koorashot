@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.match.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.matches.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('match') ? 'has-error' : '' }}">
                <label for="match">{{ trans('cruds.match.fields.match') }}</label>
                <input type="number" id="match" name="match" class="form-control" value="{{ old('match', isset($match) ? $match->match : '') }}" step="1">
                @if($errors->has('match'))
                    <p class="help-block">
                        {{ $errors->first('match') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.match.fields.match_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('league_id') ? 'has-error' : '' }}">
                <label for="league">{{ trans('cruds.match.fields.league') }}</label>
                <select name="league_id" id="league" class="form-control select2">
                    @foreach($leagues as $id => $league)
                        <option value="{{ $id }}" {{ (isset($match) && $match->league ? $match->league->id : old('league_id')) == $id ? 'selected' : '' }}>{{ $league }}</option>
                    @endforeach
                </select>
                @if($errors->has('league_id'))
                    <p class="help-block">
                        {{ $errors->first('league_id') }}
                    </p>
                @endif
            </div>
            <div class="form-group {{ $errors->has('season_id') ? 'has-error' : '' }}">
                <label for="season">{{ trans('cruds.match.fields.season') }}</label>
                <select name="season_id" id="season" class="form-control select2">
                    @foreach($seasons as $id => $season)
                        <option value="{{ $id }}" {{ (isset($match) && $match->season ? $match->season->id : old('season_id')) == $id ? 'selected' : '' }}>{{ $season }}</option>
                    @endforeach
                </select>
                @if($errors->has('season_id'))
                    <p class="help-block">
                        {{ $errors->first('season_id') }}
                    </p>
                @endif
            </div>
            <div class="form-group {{ $errors->has('stage') ? 'has-error' : '' }}">
                <label for="stage">{{ trans('cruds.match.fields.stage') }}</label>
                <input type="number" id="stage" name="stage" class="form-control" value="{{ old('stage', isset($match) ? $match->stage : '') }}" step="1">
                @if($errors->has('stage'))
                    <p class="help-block">
                        {{ $errors->first('stage') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.match.fields.stage_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('round') ? 'has-error' : '' }}">
                <label for="round">{{ trans('cruds.match.fields.round') }}</label>
                <input type="number" id="round" name="round" class="form-control" value="{{ old('round', isset($match) ? $match->round : '') }}" step="1">
                @if($errors->has('round'))
                    <p class="help-block">
                        {{ $errors->first('round') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.match.fields.round_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('group') ? 'has-error' : '' }}">
                <label for="group">{{ trans('cruds.match.fields.group') }}</label>
                <input type="number" id="group" name="group" class="form-control" value="{{ old('group', isset($match) ? $match->group : '') }}" step="1">
                @if($errors->has('group'))
                    <p class="help-block">
                        {{ $errors->first('group') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.match.fields.group_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('aggregate') ? 'has-error' : '' }}">
                <label for="aggregate">{{ trans('cruds.match.fields.aggregate') }}</label>
                <input type="number" id="aggregate" name="aggregate" class="form-control" value="{{ old('aggregate', isset($match) ? $match->aggregate : '') }}" step="1">
                @if($errors->has('aggregate'))
                    <p class="help-block">
                        {{ $errors->first('aggregate') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.match.fields.aggregate_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('venue') ? 'has-error' : '' }}">
                <label for="venue">{{ trans('cruds.match.fields.venue') }}</label>
                <input type="number" id="venue" name="venue" class="form-control" value="{{ old('venue', isset($match) ? $match->venue : '') }}" step="1">
                @if($errors->has('venue'))
                    <p class="help-block">
                        {{ $errors->first('venue') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.match.fields.venue_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('referee') ? 'has-error' : '' }}">
                <label for="referee">{{ trans('cruds.match.fields.referee') }}</label>
                <input type="number" id="referee" name="referee" class="form-control" value="{{ old('referee', isset($match) ? $match->referee : '') }}" step="1">
                @if($errors->has('referee'))
                    <p class="help-block">
                        {{ $errors->first('referee') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.match.fields.referee_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('local_team_id') ? 'has-error' : '' }}">
                <label for="local_team">{{ trans('cruds.match.fields.local_team') }}</label>
                <select name="local_team_id" id="local_team" class="form-control select2">
                    @foreach($local_teams as $id => $local_team)
                        <option value="{{ $id }}" {{ (isset($match) && $match->local_team ? $match->local_team->id : old('local_team_id')) == $id ? 'selected' : '' }}>{{ $local_team }}</option>
                    @endforeach
                </select>
                @if($errors->has('local_team_id'))
                    <p class="help-block">
                        {{ $errors->first('local_team_id') }}
                    </p>
                @endif
            </div>
            <div class="form-group {{ $errors->has('visitor_team_id') ? 'has-error' : '' }}">
                <label for="visitor_team">{{ trans('cruds.match.fields.visitor_team') }}</label>
                <select name="visitor_team_id" id="visitor_team" class="form-control select2">
                    @foreach($visitor_teams as $id => $visitor_team)
                        <option value="{{ $id }}" {{ (isset($match) && $match->visitor_team ? $match->visitor_team->id : old('visitor_team_id')) == $id ? 'selected' : '' }}>{{ $visitor_team }}</option>
                    @endforeach
                </select>
                @if($errors->has('visitor_team_id'))
                    <p class="help-block">
                        {{ $errors->first('visitor_team_id') }}
                    </p>
                @endif
            </div>
            <div class="form-group {{ $errors->has('winner_team_id') ? 'has-error' : '' }}">
                <label for="winner_team">{{ trans('cruds.match.fields.winner_team') }}</label>
                <select name="winner_team_id" id="winner_team" class="form-control select2">
                    @foreach($winner_teams as $id => $winner_team)
                        <option value="{{ $id }}" {{ (isset($match) && $match->winner_team ? $match->winner_team->id : old('winner_team_id')) == $id ? 'selected' : '' }}>{{ $winner_team }}</option>
                    @endforeach
                </select>
                @if($errors->has('winner_team_id'))
                    <p class="help-block">
                        {{ $errors->first('winner_team_id') }}
                    </p>
                @endif
            </div>
            <div class="form-group {{ $errors->has('weather_report') ? 'has-error' : '' }}">
                <label for="weather_report">{{ trans('cruds.match.fields.weather_report') }}</label>
                <input type="text" id="weather_report" name="weather_report" class="form-control" value="{{ old('weather_report', isset($match) ? $match->weather_report : '') }}">
                @if($errors->has('weather_report'))
                    <p class="help-block">
                        {{ $errors->first('weather_report') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.match.fields.weather_report_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('commentaries') ? 'has-error' : '' }}">
                <label for="commentaries">{{ trans('cruds.match.fields.commentaries') }}</label>
                <input name="commentaries" type="hidden" value="0">
                <input value="1" type="checkbox" id="commentaries" name="commentaries" {{ old('commentaries', 0) == 1 ? 'checked' : '' }}>
                @if($errors->has('commentaries'))
                    <p class="help-block">
                        {{ $errors->first('commentaries') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.match.fields.commentaries_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('attendance') ? 'has-error' : '' }}">
                <label for="attendance">{{ trans('cruds.match.fields.attendance') }}</label>
                <input type="text" id="attendance" name="attendance" class="form-control" value="{{ old('attendance', isset($match) ? $match->attendance : '') }}">
                @if($errors->has('attendance'))
                    <p class="help-block">
                        {{ $errors->first('attendance') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.match.fields.attendance_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('pitch') ? 'has-error' : '' }}">
                <label for="pitch">{{ trans('cruds.match.fields.pitch') }}</label>
                <input type="text" id="pitch" name="pitch" class="form-control" value="{{ old('pitch', isset($match) ? $match->pitch : '') }}">
                @if($errors->has('pitch'))
                    <p class="help-block">
                        {{ $errors->first('pitch') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.match.fields.pitch_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('winning_odds_calculated') ? 'has-error' : '' }}">
                <label for="winning_odds_calculated">{{ trans('cruds.match.fields.winning_odds_calculated') }}</label>
                <input name="winning_odds_calculated" type="hidden" value="0">
                <input value="1" type="checkbox" id="winning_odds_calculated" name="winning_odds_calculated" {{ old('winning_odds_calculated', 0) == 1 ? 'checked' : '' }}>
                @if($errors->has('winning_odds_calculated'))
                    <p class="help-block">
                        {{ $errors->first('winning_odds_calculated') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.match.fields.winning_odds_calculated_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('formations') ? 'has-error' : '' }}">
                <label for="formations">{{ trans('cruds.match.fields.formations') }}</label>
                <input type="text" id="formations" name="formations" class="form-control" value="{{ old('formations', isset($match) ? $match->formations : '') }}">
                @if($errors->has('formations'))
                    <p class="help-block">
                        {{ $errors->first('formations') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.match.fields.formations_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('scores') ? 'has-error' : '' }}">
                <label for="scores">{{ trans('cruds.match.fields.scores') }}</label>
                <input type="text" id="scores" name="scores" class="form-control" value="{{ old('scores', isset($match) ? $match->scores : '') }}">
                @if($errors->has('scores'))
                    <p class="help-block">
                        {{ $errors->first('scores') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.match.fields.scores_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('time') ? 'has-error' : '' }}">
                <label for="time">{{ trans('cruds.match.fields.time') }}</label>
                <input type="text" id="time" name="time" class="form-control" value="{{ old('time', isset($match) ? $match->time : '') }}">
                @if($errors->has('time'))
                    <p class="help-block">
                        {{ $errors->first('time') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.match.fields.time_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('coaches') ? 'has-error' : '' }}">
                <label for="coaches">{{ trans('cruds.match.fields.coaches') }}</label>
                <input type="text" id="coaches" name="coaches" class="form-control" value="{{ old('coaches', isset($match) ? $match->coaches : '') }}">
                @if($errors->has('coaches'))
                    <p class="help-block">
                        {{ $errors->first('coaches') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.match.fields.coaches_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('standings') ? 'has-error' : '' }}">
                <label for="standings">{{ trans('cruds.match.fields.standings') }}</label>
                <input type="text" id="standings" name="standings" class="form-control" value="{{ old('standings', isset($match) ? $match->standings : '') }}">
                @if($errors->has('standings'))
                    <p class="help-block">
                        {{ $errors->first('standings') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.match.fields.standings_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('colors') ? 'has-error' : '' }}">
                <label for="colors">{{ trans('cruds.match.fields.colors') }}</label>
                <input type="text" id="colors" name="colors" class="form-control" value="{{ old('colors', isset($match) ? $match->colors : '') }}">
                @if($errors->has('colors'))
                    <p class="help-block">
                        {{ $errors->first('colors') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.match.fields.colors_helper') }}
                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>
@endsection