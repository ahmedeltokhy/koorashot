@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.league.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.leagues.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('sm') ? 'has-error' : '' }}">
                <label for="sm">{{ trans('cruds.league.fields.sm') }}</label>
                <input type="number" id="sm" name="sm" class="form-control" value="{{ old('sm', isset($league) ? $league->sm : '') }}" step="1">
                @if($errors->has('sm'))
                    <p class="help-block">
                        {{ $errors->first('sm') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.league.fields.sm_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('active') ? 'has-error' : '' }}">
                <label for="active">{{ trans('cruds.league.fields.active') }}</label>
                <input name="active" type="hidden" value="0">
                <input value="1" type="checkbox" id="active" name="active" {{ old('active', 0) == 1 ? 'checked' : '' }}>
                @if($errors->has('active'))
                    <p class="help-block">
                        {{ $errors->first('active') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.league.fields.active_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('country_id') ? 'has-error' : '' }}">
                <label for="country">{{ trans('cruds.league.fields.country') }}</label>
                <select name="country_id" id="country" class="form-control select2">
                    @foreach($countries as $id => $country)
                        <option value="{{ $id }}" {{ (isset($league) && $league->country ? $league->country->id : old('country_id')) == $id ? 'selected' : '' }}>{{ $country }}</option>
                    @endforeach
                </select>
                @if($errors->has('country_id'))
                    <p class="help-block">
                        {{ $errors->first('country_id') }}
                    </p>
                @endif
            </div>
            <div class="form-group {{ $errors->has('logo') ? 'has-error' : '' }}">
                <label for="logo">{{ trans('cruds.league.fields.logo') }}</label>
                <div class="needsclick dropzone" id="logo-dropzone">

                </div>
                @if($errors->has('logo'))
                    <p class="help-block">
                        {{ $errors->first('logo') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.league.fields.logo_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">{{ trans('cruds.league.fields.name') }}</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($league) ? $league->name : '') }}">
                @if($errors->has('name'))
                    <p class="help-block">
                        {{ $errors->first('name') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.league.fields.name_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('name_en') ? 'has-error' : '' }}">
                <label for="name_en">{{ trans('cruds.league.fields.name_en') }}</label>
                <input type="text" id="name_en" name="name_en" class="form-control" value="{{ old('name_en', isset($league) ? $league->name_en : '') }}">
                @if($errors->has('name_en'))
                    <p class="help-block">
                        {{ $errors->first('name_en') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.league.fields.name_en_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('is_cup') ? 'has-error' : '' }}">
                <label for="is_cup">{{ trans('cruds.league.fields.is_cup') }}</label>
                <input name="is_cup" type="hidden" value="0">
                <input value="1" type="checkbox" id="is_cup" name="is_cup" {{ old('is_cup', 0) == 1 ? 'checked' : '' }}>
                @if($errors->has('is_cup'))
                    <p class="help-block">
                        {{ $errors->first('is_cup') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.league.fields.is_cup_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('current_season') ? 'has-error' : '' }}">
                <label for="current_season">{{ trans('cruds.league.fields.current_season') }}</label>
                <input type="text" id="current_season" name="current_season" class="form-control" value="{{ old('current_season', isset($league) ? $league->current_season : '') }}">
                @if($errors->has('current_season'))
                    <p class="help-block">
                        {{ $errors->first('current_season') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.league.fields.current_season_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('current_round') ? 'has-error' : '' }}">
                <label for="current_round">{{ trans('cruds.league.fields.current_round') }}</label>
                <input type="text" id="current_round" name="current_round" class="form-control" value="{{ old('current_round', isset($league) ? $league->current_round : '') }}">
                @if($errors->has('current_round'))
                    <p class="help-block">
                        {{ $errors->first('current_round') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.league.fields.current_round_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('current_stage') ? 'has-error' : '' }}">
                <label for="current_stage">{{ trans('cruds.league.fields.current_stage') }}</label>
                <input type="text" id="current_stage" name="current_stage" class="form-control" value="{{ old('current_stage', isset($league) ? $league->current_stage : '') }}">
                @if($errors->has('current_stage'))
                    <p class="help-block">
                        {{ $errors->first('current_stage') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.league.fields.current_stage_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('live_standings') ? 'has-error' : '' }}">
                <label for="live_standings">{{ trans('cruds.league.fields.live_standings') }}</label>
                <input name="live_standings" type="hidden" value="0">
                <input value="1" type="checkbox" id="live_standings" name="live_standings" {{ old('live_standings', 0) == 1 ? 'checked' : '' }}>
                @if($errors->has('live_standings'))
                    <p class="help-block">
                        {{ $errors->first('live_standings') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.league.fields.live_standings_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('predictions') ? 'has-error' : '' }}">
                <label for="predictions">{{ trans('cruds.league.fields.predictions') }}</label>
                <input name="predictions" type="hidden" value="0">
                <input value="1" type="checkbox" id="predictions" name="predictions" {{ old('predictions', 0) == 1 ? 'checked' : '' }}>
                @if($errors->has('predictions'))
                    <p class="help-block">
                        {{ $errors->first('predictions') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.league.fields.predictions_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('topscorer_assists') ? 'has-error' : '' }}">
                <label for="topscorer_assists">{{ trans('cruds.league.fields.topscorer_assists') }}</label>
                <input name="topscorer_assists" type="hidden" value="0">
                <input value="1" type="checkbox" id="topscorer_assists" name="topscorer_assists" {{ old('topscorer_assists', 0) == 1 ? 'checked' : '' }}>
                @if($errors->has('topscorer_assists'))
                    <p class="help-block">
                        {{ $errors->first('topscorer_assists') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.league.fields.topscorer_assists_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('topscorer_cards') ? 'has-error' : '' }}">
                <label for="topscorer_cards">{{ trans('cruds.league.fields.topscorer_cards') }}</label>
                <input name="topscorer_cards" type="hidden" value="0">
                <input value="1" type="checkbox" id="topscorer_cards" name="topscorer_cards" {{ old('topscorer_cards', 0) == 1 ? 'checked' : '' }}>
                @if($errors->has('topscorer_cards'))
                    <p class="help-block">
                        {{ $errors->first('topscorer_cards') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.league.fields.topscorer_cards_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('topscorer_goals') ? 'has-error' : '' }}">
                <label for="topscorer_goals">{{ trans('cruds.league.fields.topscorer_goals') }}</label>
                <input name="topscorer_goals" type="hidden" value="0">
                <input value="1" type="checkbox" id="topscorer_goals" name="topscorer_goals" {{ old('topscorer_goals', 0) == 1 ? 'checked' : '' }}>
                @if($errors->has('topscorer_goals'))
                    <p class="help-block">
                        {{ $errors->first('topscorer_goals') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.league.fields.topscorer_goals_helper') }}
                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
<script>
    Dropzone.options.logoDropzone = {
    url: '{{ route('admin.leagues.storeMedia') }}',
    maxFilesize: 2, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 2,
      width: 4096,
      height: 4096
    },
    success: function (file, response) {
      $('form').find('input[name="logo"]').remove()
      $('form').append('<input type="hidden" name="logo" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      $('form').find('input[name="logo"]').remove()
      this.options.maxFiles = this.options.maxFiles + 1
    },
    init: function () {
@if(isset($league) && $league->logo)
      var file = {!! json_encode($league->logo) !!}
          this.options.addedfile.call(this, file)
      this.options.thumbnail.call(this, file, file.url)
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="logo" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
    error: function (file, response) {
        if ($.type(response) === 'string') {
            var message = response //dropzone sends it's own error messages in string
        } else {
            var message = response.errors.file
        }
        file.previewElement.classList.add('dz-error')
        _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
        _results = []
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i]
            _results.push(node.textContent = message)
        }

        return _results
    }
}
</script>
@stop