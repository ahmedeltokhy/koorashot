@extends('layouts.admin')
@section('content')
@can('league_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.leagues.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.league.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.league.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.league.fields.sm') }}
                        </th>
                        <th>
                            {{ trans('cruds.league.fields.active') }}
                        </th>
                        <th>
                            {{ trans('cruds.league.fields.country') }}
                        </th>
                        <th>
                            {{ trans('cruds.league.fields.logo') }}
                        </th>
                        <th>
                            {{ trans('cruds.league.fields.name') }}
                        </th>
                        <th>
                            {{ trans('cruds.league.fields.name_en') }}
                        </th>
                        <th>
                            {{ trans('cruds.league.fields.is_cup') }}
                        </th>
                        <th>
                            {{ trans('cruds.league.fields.current_season') }}
                        </th>
                        <th>
                            {{ trans('cruds.league.fields.current_round') }}
                        </th>
                        <th>
                            {{ trans('cruds.league.fields.current_stage') }}
                        </th>
                        <th>
                            {{ trans('cruds.league.fields.live_standings') }}
                        </th>
                        <th>
                            {{ trans('cruds.league.fields.predictions') }}
                        </th>
                        <th>
                            {{ trans('cruds.league.fields.topscorer_assists') }}
                        </th>
                        <th>
                            {{ trans('cruds.league.fields.topscorer_cards') }}
                        </th>
                        <th>
                            {{ trans('cruds.league.fields.topscorer_goals') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($leagues as $key => $league)
                        <tr data-entry-id="{{ $league->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $league->sm ?? '' }}
                            </td>
                            <td>
                                {{ $league->active ? trans('global.yes') : trans('global.no') }}
                            </td>
                            <td>
                                {{ $league->country->name ?? '' }}
                            </td>
                            <td>
                                @if($league->logo)
                                    <a href="{{ $league->logo->getUrl() }}" target="_blank">
                                        <img src="{{ $league->logo->getUrl('thumb') }}" width="50px" height="50px">
                                    </a>
                                @endif
                            </td>
                            <td>
                                {{ $league->name ?? '' }}
                            </td>
                            <td>
                                {{ $league->name_en ?? '' }}
                            </td>
                            <td>
                                {{ $league->is_cup ? trans('global.yes') : trans('global.no') }}
                            </td>
                            <td>
                                {{ $league->current_season ?? '' }}
                            </td>
                            <td>
                                {{ $league->current_round ?? '' }}
                            </td>
                            <td>
                                {{ $league->current_stage ?? '' }}
                            </td>
                            <td>
                                {{ $league->live_standings ? trans('global.yes') : trans('global.no') }}
                            </td>
                            <td>
                                {{ $league->predictions ? trans('global.yes') : trans('global.no') }}
                            </td>
                            <td>
                                {{ $league->topscorer_assists ? trans('global.yes') : trans('global.no') }}
                            </td>
                            <td>
                                {{ $league->topscorer_cards ? trans('global.yes') : trans('global.no') }}
                            </td>
                            <td>
                                {{ $league->topscorer_goals ? trans('global.yes') : trans('global.no') }}
                            </td>
                            <td>
                                @can('league_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.leagues.show', $league->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan
                                @can('league_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.leagues.edit', $league->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
                                @can('league_delete')
                                    <form action="{{ route('admin.leagues.destroy', $league->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.leagues.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
@can('league_delete')
  dtButtons.push(deleteButton)
@endcan

  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection