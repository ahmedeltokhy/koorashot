@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.league.title') }}
    </div>

    <div class="card-body">
        <div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.league.fields.sm') }}
                        </th>
                        <td>
                            {{ $league->sm }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.league.fields.active') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled {{ $league->active ? "checked" : "" }}>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.league.fields.country') }}
                        </th>
                        <td>
                            {{ $league->country->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.league.fields.logo') }}
                        </th>
                        <td>
                            @if($league->logo)
                                <a href="{{ $league->logo->getUrl() }}" target="_blank">
                                    <img src="{{ $league->logo->getUrl('thumb') }}" width="50px" height="50px">
                                </a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.league.fields.name') }}
                        </th>
                        <td>
                            {{ $league->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.league.fields.name_en') }}
                        </th>
                        <td>
                            {{ $league->name_en }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.league.fields.is_cup') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled {{ $league->is_cup ? "checked" : "" }}>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.league.fields.current_season') }}
                        </th>
                        <td>
                            {{ $league->current_season }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.league.fields.current_round') }}
                        </th>
                        <td>
                            {{ $league->current_round }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.league.fields.current_stage') }}
                        </th>
                        <td>
                            {{ $league->current_stage }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.league.fields.live_standings') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled {{ $league->live_standings ? "checked" : "" }}>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.league.fields.predictions') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled {{ $league->predictions ? "checked" : "" }}>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.league.fields.topscorer_assists') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled {{ $league->topscorer_assists ? "checked" : "" }}>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.league.fields.topscorer_cards') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled {{ $league->topscorer_cards ? "checked" : "" }}>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.league.fields.topscorer_goals') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled {{ $league->topscorer_goals ? "checked" : "" }}>
                        </td>
                    </tr>
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>
    </div>
</div>
@endsection