@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.filter.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.filters.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.filter.fields.id') }}
                        </th>
                        <td>
                            {{ $filter->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.filter.fields.imageable') }}
                        </th>
                        <td>
                            {{ $filter->imageable }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.filter.fields.imageable_type') }}
                        </th>
                        <td>
                            {{ $filter->imageable_type }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.filter.fields.image') }}
                        </th>
                        <td>
                            @if($filter->image)
                                <a href="{{ $filter->image->getUrl() }}" target="_blank">
                                    <img src="{{ $filter->image->getUrl('thumb') }}" width="50px" height="50px">
                                </a>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.filters.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection