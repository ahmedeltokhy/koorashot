<?php
use Illuminate\Support\Facades\Route;
Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:api']], function () {
    Route::apiResource('permissions', 'PermissionsApiController');
    Route::apiResource('roles', 'RolesApiController');
    Route::apiResource('users', 'UsersApiController');
    Route::apiResource('user-devices', 'UserDeviceApiController');
    Route::apiResource('continents', 'ContinentApiController');
    Route::apiResource('countries', 'CountryApiController');
    Route::apiResource('leagues', 'LeagueApiController');
    Route::apiResource('seasons', 'SeasonApiController');
    Route::apiResource('teams', 'TeamApiController');
    Route::apiResource('matches', 'MatchApiController');
});
Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Mobile', 'middleware' => ['auth:api']], function () {
    Route::post('register_token',['as'=>'register_token','uses'=>'UsersController@register_token']);
    Route::post('logout',['as'=>'logout','uses'=>'UsersController@logout']);
    Route::post('remove_token',['as'=>'remove_token','uses'=>'UsersController@remove_token']);

});
Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Mobile'], function () {
Route::post('register',['as'=>'register','uses'=>'UsersController@register']);
Route::post('login',['as'=>'login','uses'=>'UsersController@login']);
Route::post('reset',['as'=>'reset','uses'=>'UsersController@reset']);
//
Route::group(['prefix' => 'database'], function () {
Route::get('continents','ContinentController@getAllContinents');
Route::get('countries','CountryController@getAllCountries');
Route::get('leagues','LeagueController@recordLeagues');
Route::get('seasons','SeasonController@recordSeasons');
Route::get('teams/record','TeamController@recordTeams');
Route::get('matches/record','MatchController@recordMatches');
});
//matches
Route::get('matches/{id}/show','MatchController@getById');
//seasons
Route::get('seasons','SeasonController@getSeasons');
//leagues
Route::get('leagues','LeagueController@getLeagues');
//filters
Route::post('filters/search','FilterController@getFilters');
Route::post('filters/match','FilterController@getMatchFilters');

Route::get('matches','MatchController@index');
Route::post('matches/date','MatchController@getByDate');
Route::post('matches/date/range','MatchController@getByDateRange');



Route::group(['middleware'=>'apiToken'],function(){
    Route::post('teams/follow','TeamController@followTeam');
    Route::get('user/followed/teams','TeamController@myFollowedTeam');
    //news sources
    Route::get('news/sources','SourceController@getNewsSources');
    //follow source of news
    Route::post('news/sources/follow','SourceController@followSource');
    //teams
    Route::post('teams','TeamController@getTeams');
    Route::post('teams/search','TeamController@getTeamByName');
    //matches
    Route::post('matches/date','MatchController@getByDateRange');
    //news
    Route::post('news','NewsController@getNews');
    
    

});

    Route::get('api_check', 'UsersController@api_check')->name('api_check');

});
