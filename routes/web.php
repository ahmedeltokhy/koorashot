<?php

Route::redirect('/', '/login');
Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes(['register' => false]);
// Admin

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // User Devices
    Route::delete('user-devices/destroy', 'UserDeviceController@massDestroy')->name('user-devices.massDestroy');
    Route::resource('user-devices', 'UserDeviceController');

    // Continents
    Route::delete('continents/destroy', 'ContinentController@massDestroy')->name('continents.massDestroy');
    Route::resource('continents', 'ContinentController');

    // Countries
    Route::delete('countries/destroy', 'CountryController@massDestroy')->name('countries.massDestroy');
    Route::resource('countries', 'CountryController');

    // Leagues
    Route::delete('leagues/destroy', 'LeagueController@massDestroy')->name('leagues.massDestroy');
    Route::post('leagues/media', 'LeagueController@storeMedia')->name('leagues.storeMedia');
    Route::post('leagues/ckmedia', 'LeagueController@storeCKEditorImages')->name('leagues.storeCKEditorImages');
    Route::resource('leagues', 'LeagueController');

    // Seasons
    Route::delete('seasons/destroy', 'SeasonController@massDestroy')->name('seasons.massDestroy');
    Route::resource('seasons', 'SeasonController');

    // Teams
    Route::delete('teams/destroy', 'TeamController@massDestroy')->name('teams.massDestroy');
    Route::resource('teams', 'TeamController');

    // Matches
    Route::delete('matches/destroy', 'MatchController@massDestroy')->name('matches.massDestroy');
    Route::resource('matches', 'MatchController');

    // Filters
    Route::delete('filters/destroy', 'FilterController@massDestroy')->name('filters.massDestroy');
    Route::post('filters/media', 'FilterController@storeMedia')->name('filters.storeMedia');
    Route::post('filters/ckmedia', 'FilterController@storeCKEditorImages')->name('filters.storeCKEditorImages');
    Route::resource('filters', 'FilterController');
});


\Illuminate\Support\Facades\Route::any('download', ['as' => 'download', 'uses' => 'Api\V1\Mobile\UsersController@download']);
