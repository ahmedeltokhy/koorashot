<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [[
            'id'         => '1',
            'title'      => 'user_management_access',
            'created_at' => '2019-08-05 08:35:57',
            'updated_at' => '2019-08-05 08:35:57',
        ],
            [
                'id'         => '2',
                'title'      => 'permission_create',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '3',
                'title'      => 'permission_edit',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '4',
                'title'      => 'permission_show',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '5',
                'title'      => 'permission_delete',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '6',
                'title'      => 'permission_access',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '7',
                'title'      => 'role_create',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '8',
                'title'      => 'role_edit',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '9',
                'title'      => 'role_show',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '10',
                'title'      => 'role_delete',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '11',
                'title'      => 'role_access',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '12',
                'title'      => 'user_create',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '13',
                'title'      => 'user_edit',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '14',
                'title'      => 'user_show',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '15',
                'title'      => 'user_delete',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '16',
                'title'      => 'user_access',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '17',
                'title'      => 'user_device_create',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '18',
                'title'      => 'user_device_edit',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '19',
                'title'      => 'user_device_show',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '20',
                'title'      => 'user_device_delete',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '21',
                'title'      => 'user_device_access',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '22',
                'title'      => 'data_access',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '23',
                'title'      => 'continent_create',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '24',
                'title'      => 'continent_edit',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '25',
                'title'      => 'continent_show',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '26',
                'title'      => 'continent_delete',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '27',
                'title'      => 'continent_access',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '28',
                'title'      => 'country_create',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '29',
                'title'      => 'country_edit',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '30',
                'title'      => 'country_show',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '31',
                'title'      => 'country_delete',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '32',
                'title'      => 'country_access',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '33',
                'title'      => 'league_create',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '34',
                'title'      => 'league_edit',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '35',
                'title'      => 'league_show',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '36',
                'title'      => 'league_delete',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '37',
                'title'      => 'league_access',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '38',
                'title'      => 'season_create',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '39',
                'title'      => 'season_edit',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '40',
                'title'      => 'season_show',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '41',
                'title'      => 'season_delete',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '42',
                'title'      => 'season_access',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '43',
                'title'      => 'team_create',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '44',
                'title'      => 'team_edit',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '45',
                'title'      => 'team_show',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '46',
                'title'      => 'team_delete',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '47',
                'title'      => 'team_access',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '48',
                'title'      => 'match_create',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '49',
                'title'      => 'match_edit',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '50',
                'title'      => 'match_show',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '51',
                'title'      => 'match_delete',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ],
            [
                'id'         => '52',
                'title'      => 'match_access',
                'created_at' => '2019-08-05 08:35:57',
                'updated_at' => '2019-08-05 08:35:57',
            ]];

        Permission::insert($permissions);
    }
}
