<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [[
            'id'             => 1,
            'name'           => 'Admin',
            'email'          => 'admin@admin.com',
            'password'       => '$2y$10$2GPW11A8YY240Vj8Zhs7HeQ.DOhTF2uuD5MW.fy/V8KRxsuLSA5PO',
            'remember_token' => null,
            'created_at'     => '2019-08-05 08:35:55',
            'updated_at'     => '2019-08-05 08:35:55',
            'deleted_at'     => null,
            'device'         => '',
        ]];

        User::insert($users);
    }
}
