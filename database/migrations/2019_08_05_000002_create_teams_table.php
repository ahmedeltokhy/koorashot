<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamsTable extends Migration
{
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sm')->nullable();
            $table->string('name')->nullable();
            $table->string('name_en')->nullable();
            $table->string('short_code')->nullable();
            $table->string('twitter')->nullable();
            $table->boolean('national_team')->default(0)->nullable();
            $table->integer('founded')->nullable();
            $table->string('logo')->nullable();
            $table->integer('venue')->nullable();
            $table->integer('current_season')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
