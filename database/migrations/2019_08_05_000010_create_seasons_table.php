<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeasonsTable extends Migration
{
    public function up()
    {
        Schema::create('seasons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sm')->nullable();
            $table->string('name')->nullable();
            $table->boolean('is_current_season')->default(0)->nullable();
            $table->integer('current_round')->nullable();
            $table->integer('current_stage')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
