<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToMatchesTable extends Migration
{
    public function up()
    {
        Schema::table('matches', function (Blueprint $table) {
            $table->unsignedInteger('league_id')->nullable();
            $table->foreign('league_id', 'league_fk_220001')->references('id')->on('leagues');
            $table->unsignedInteger('season_id')->nullable();
            $table->foreign('season_id', 'season_fk_220002')->references('id')->on('seasons');
            $table->unsignedInteger('local_team_id')->nullable();
            $table->foreign('local_team_id', 'local_team_fk_220009')->references('id')->on('teams');
            $table->unsignedInteger('visitor_team_id')->nullable();
            $table->foreign('visitor_team_id', 'visitor_team_fk_220010')->references('id')->on('teams');
            $table->unsignedInteger('winner_team_id')->nullable();
            $table->foreign('winner_team_id', 'winner_team_fk_220011')->references('id')->on('teams');
        });
    }
}
