<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Create1564994156966MatchesTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('matches')) {
            Schema::create('matches', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('match')->nullable();
                $table->unsignedInteger('league_id')->nullable();
                $table->foreign('league_id', 'league_fk_220001')->references('id')->on('leagues');
                $table->unsignedInteger('season_id')->nullable();
                $table->foreign('season_id', 'season_fk_220002')->references('id')->on('seasons');
                $table->integer('stage')->nullable();
                $table->integer('round')->nullable();
                $table->integer('group')->nullable();
                $table->integer('aggregate')->nullable();
                $table->integer('venue')->nullable();
                $table->integer('referee')->nullable();
                $table->unsignedInteger('local_team_id')->nullable();
                $table->foreign('local_team_id', 'local_team_fk_220009')->references('id')->on('teams');
                $table->unsignedInteger('visitor_team_id')->nullable();
                $table->foreign('visitor_team_id', 'visitor_team_fk_220010')->references('id')->on('teams');
                $table->unsignedInteger('winner_team_id')->nullable();
                $table->foreign('winner_team_id', 'winner_team_fk_220011')->references('id')->on('teams');
                $table->string('weather_report')->nullable();
                $table->boolean('commentaries')->default(0)->nullable();
                $table->string('attendance')->nullable();
                $table->string('pitch')->nullable();
                $table->boolean('winning_odds_calculated')->default(0)->nullable();
                $table->string('formations')->nullable();
                $table->string('scores')->nullable();
                $table->string('time')->nullable();
                $table->string('coaches')->nullable();
                $table->string('standings')->nullable();
                $table->string('colors')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
