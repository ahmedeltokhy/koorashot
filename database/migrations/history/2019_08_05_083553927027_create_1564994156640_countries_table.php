<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Create1564994156640CountriesTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('countries')) {
            Schema::create('countries', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name')->nullable();
                $table->string('name_en')->nullable();
                $table->unsignedInteger('continent_id')->nullable();
                $table->foreign('continent_id', 'continent_fk_219942')->references('id')->on('continents');
                $table->string('sub_region')->nullable();
                $table->string('world_region')->nullable();
                $table->string('fifa')->nullable();
                $table->string('iso')->nullable();
                $table->string('iso_2')->nullable();
                $table->string('lat')->nullable();
                $table->string('lng')->nullable();
                $table->longText('flag')->nullable();
                $table->integer('sm')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
