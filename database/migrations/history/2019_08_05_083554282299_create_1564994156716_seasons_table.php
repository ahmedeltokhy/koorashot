<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Create1564994156716SeasonsTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('seasons')) {
            Schema::create('seasons', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('sm')->nullable();
                $table->string('name')->nullable();
                $table->unsignedInteger('league_id')->nullable();
                $table->foreign('league_id', 'league_fk_219977')->references('id')->on('leagues');
                $table->boolean('is_current_season')->default(0)->nullable();
                $table->integer('current_round')->nullable();
                $table->integer('current_stage')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('seasons');
    }
}
