<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToSeasonsTable extends Migration
{
    public function up()
    {
        Schema::table('seasons', function (Blueprint $table) {
            $table->unsignedInteger('league_id')->nullable();
            $table->foreign('league_id', 'league_fk_219977')->references('id')->on('leagues');
        });
    }
}
