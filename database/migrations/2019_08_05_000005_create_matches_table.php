<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchesTable extends Migration
{
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('match')->nullable();
            $table->integer('stage')->nullable();
            $table->integer('round')->nullable();
            $table->integer('group')->nullable();
            $table->integer('aggregate')->nullable();
            $table->integer('venue')->nullable();
            $table->integer('referee')->nullable();
            $table->string('weather_report')->nullable();
            $table->boolean('commentaries')->default(0)->nullable();
            $table->string('attendance')->nullable();
            $table->string('pitch')->nullable();
            $table->boolean('winning_odds_calculated')->default(0)->nullable();
            $table->string('formations')->nullable();
            $table->string('scores')->nullable();
            $table->string('time')->nullable();
            $table->string('coaches')->nullable();
            $table->string('standings')->nullable();
            $table->string('colors')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
