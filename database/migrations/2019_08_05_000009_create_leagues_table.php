<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaguesTable extends Migration
{
    public function up()
    {
        Schema::create('leagues', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sm')->nullable();
            $table->boolean('active')->default(0)->nullable();
            $table->string('name')->nullable();
            $table->string('name_en')->nullable();
            $table->boolean('is_cup')->default(0)->nullable();
            $table->string('current_season')->nullable();
            $table->string('current_round')->nullable();
            $table->string('current_stage')->nullable();
            $table->boolean('live_standings')->default(0)->nullable();
            $table->boolean('predictions')->default(0)->nullable();
            $table->boolean('topscorer_assists')->default(0)->nullable();
            $table->boolean('topscorer_cards')->default(0)->nullable();
            $table->boolean('topscorer_goals')->default(0)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
