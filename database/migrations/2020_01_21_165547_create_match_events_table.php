<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('player_id')->nullable();
            $table->unsignedInteger('minute')->nullable();
            $table->string('result')->nullable();
            $table->string('type')->nullable();
            $table->string('player_name')->nullable();
            $table->unsignedInteger('related_player_id')->nullable();
            $table->string('related_player_name')->nullable();
            $table->unsignedInteger('match_id');
            $table->foreign('match_id')->references('id')->on('matches');
            $table->unsignedInteger('team_id');
            $table->foreign('team_id')->references('id')->on('teams');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('match_events');
    }
}
